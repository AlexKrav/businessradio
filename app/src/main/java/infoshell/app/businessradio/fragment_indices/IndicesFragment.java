package infoshell.app.businessradio.fragment_indices;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import infoshell.app.businessradio.R;
import infoshell.app.businessradio.databinding.FragmentIndicesBinding;

public class IndicesFragment extends Fragment implements IndicesFragmentIntefcace {
    private FragmentIndicesBinding binding;
    private IndicesFragmentPresenterInterface presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_indices, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new IndicesFragmentPresenter(this);
    }
}
