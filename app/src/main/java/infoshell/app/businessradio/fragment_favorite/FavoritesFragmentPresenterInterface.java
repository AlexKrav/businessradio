package infoshell.app.businessradio.fragment_favorite;

import java.util.List;

import infoshell.app.businessradio.models.StationModel;
import infoshell.app.businessradio.models.StationViewModel;

public interface FavoritesFragmentPresenterInterface {

    void onViewCreated();

    void setUserVisibleHint();

    void onItemClick(StationModel station);

    void onItemMoved(List<StationModel> stationFavoriteList);

    void onFavoriteClick(StationModel station);

    void updateFavoriteViewModel(StationModel station);

    void onChangedPlayerState(StationViewModel stationViewModel);

    void onDestroy();

}
