package infoshell.app.businessradio.fragment_favorite;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Objects;

import infoshell.app.businessradio.R;
import infoshell.app.businessradio.adapters.FavoritesFragmentAdapter;
import infoshell.app.businessradio.databinding.FragmentFavoritesBinding;
import infoshell.app.businessradio.helpers.Constants;
import infoshell.app.businessradio.helpers.DialogUtility;
import infoshell.app.businessradio.helpers.InternetConnectingManager;
import infoshell.app.businessradio.helpers.PlayerControllerListener;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.helpers.SwipeAndDragHelper;
import infoshell.app.businessradio.models.FavoriteStarModel;
import infoshell.app.businessradio.models.RadioMessageEvent;
import infoshell.app.businessradio.models.SharedViewModel;
import infoshell.app.businessradio.models.StationModel;
import infoshell.app.businessradio.models.StationViewModel;

public class FavoritesFragment extends Fragment implements FavoritesFragmentInterface {
    private FragmentFavoritesBinding binding;
    private FavoritesFragmentPresenterInterface presenter;
    private FavoritesFragmentAdapter adapter;
    private MutableLiveData<StationViewModel> stationModelLiveData = new MutableLiveData<>();
    private SharedViewModel sharedViewModel;
    private PlayerControllerListener playerControllerListener;
    private StationModel previousStationModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter = new FavoritesFragmentPresenter(this);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorites, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();


        presenter.onViewCreated();
        binding.rvFavorites.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PlayerControllerListener) {
            playerControllerListener = (PlayerControllerListener) context;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (presenter != null && isVisibleToUser) {
            presenter.setUserVisibleHint();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void showFavoriteStationList(List<StationModel> stations) {
        if (stations.size() > 0) {
            adapter.refill(stations);
            binding.tvFavorite.setVisibility(View.INVISIBLE);
        } else {
            binding.tvFavorite.setVisibility(View.VISIBLE);
            adapter.clearStations();
        }
    }

    @Override
    public void startService(Intent intent) {
        Objects.requireNonNull(getActivity()).startService(intent);
    }

    @Override
    public void sendRadioMessageEventBus(String stationId, boolean isServiceStarted) {
        RadioMessageEvent radioMessageEvent = new RadioMessageEvent();
        radioMessageEvent.setstationID(stationId);
        radioMessageEvent.setOnAir(isServiceStarted);
        EventBus.getDefault().post(radioMessageEvent);
    }

    @Override
    public void internetNotConnected() {
        DialogUtility.getAlertDialog(getContext(), getResources().getString(R.string.dialog_title_text), getResources().getString(R.string.internet_not_connected), getResources().getString(R.string.dialog_btn_ok), new DialogUtility.OnAlertButtonClick() {
            @Override
            public void positiveClick(DialogInterface dialogInterface) {
                super.positiveClick(dialogInterface);
            }
        }).show();
    }

    @Override
    public void startPlayerService() {
        if (InternetConnectingManager.isInternetConnected()) {
            if (playerControllerListener != null) {
                playerControllerListener.startPlayerService();
            }
        } else internetNotConnected();
    }

    @Override
    public void pausePlayerService() {
        if (InternetConnectingManager.isInternetConnected()) {
            if (playerControllerListener != null) {
                playerControllerListener.pausePlayerService();
            }
        } else internetNotConnected();
        StationViewModel stationViewModel = new StationViewModel();
        if (SharedPreferencesHelper.getSelectedStation() == null) {
            stationViewModel.setStation(SharedPreferencesHelper.getLastPlayedStation());
        } else stationViewModel.setStation(SharedPreferencesHelper.getSelectedStation());
        stationViewModel.setPlaying(false);
        stationModelLiveData.setValue(stationViewModel);
    }

    @Override
    public void changeStationState(StationModel station) {
        StationModel previousStation = sharedViewModel.getStationViewModel().getValue();
        if (previousStation != null && station.getId() == previousStation.getId() & previousStation.isPlaying()) {
            station.setPlaying(false);
            station.setPaused(true);
        } else {
            station.setPlaying(true);
            station.setPaused(false);
        }
        if (InternetConnectingManager.isInternetConnected()) {
            if (station.isPlaying()) {
                startPlayerService();
            } else {
                pausePlayerService();
            }
        } else internetNotConnected();
        sharedViewModel.setStationViewModel(station);
    }

    @Override
    public void changeRecordingState(boolean isRecording) {
        StationModel stationModel = sharedViewModel.getStationViewModel().getValue();
        if (stationModel == null) return;
        if (isRecording) {
            stationModel.setRecording(true);
        } else {
            stationModel.setRecording(false);
        }
        sharedViewModel.setStationViewModel(stationModel);
    }

    @Override
    public void changeFavoriteStarColor(StationModel station) {
        FavoriteStarModel favoriteStarModel = new FavoriteStarModel();
//        FavoriteStarEvent favoriteEvent = new FavoriteStarEvent();
        station.favorite = false;
        favoriteStarModel.setStation(station);
        sharedViewModel.setFavoriteStarModel(favoriteStarModel);
//        EventBus.getDefault().post(favoriteEvent);
    }

    private void initViews() {
        adapter = new FavoritesFragmentAdapter();
        binding.rvFavorites.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rvFavorites.setLayoutManager(llm);

        SwipeAndDragHelper swipeAndDragHelper = new SwipeAndDragHelper(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeAndDragHelper);
        itemTouchHelper.attachToRecyclerView(binding.rvFavorites);
        adapter.setTouchHelper(itemTouchHelper);
        RecyclerView.ItemAnimator animator = binding.rvFavorites.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        adapter.setOnItemClickListener(presenter::onItemClick);
        adapter.setItemMovedListener(presenter::onItemMoved);
        adapter.setOnItemStarClickListener(presenter::onFavoriteClick);
//        binding.rvFavorites.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
//            @Override
//            public boolean onInterceptTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {
//                switch (motionEvent.getActionMasked()) {
//                    case MotionEvent.ACTION_DOWN:
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        break;
//
//                }
//                return false;
//            }
//
//            @Override
//            public void onTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {
//
//            }
//
//            @Override
//            public void onRequestDisallowInterceptTouchEvent(boolean b) {
//            }
//        });
        setupObservers();
    }

    private void setupObservers() {
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel.class);
        sharedViewModel.getStationViewModel().observe(getViewLifecycleOwner(), stationModel -> {
            if (stationModel == null) return;
            for (StationModel model : adapter.getStationList()) {
                if (stationModel.getId() == model.getId()) {
                    model.setPlaying(stationModel.isPlaying());
                    model.setPaused(stationModel.isPaused());
                    adapter.notifyItemChanged(adapter.getStationList().indexOf(model));
                    presenter.updateFavoriteViewModel(model);
                } else if (model.isPlaying() || model.isPaused()) {
                    model.setPlaying(false);
                    model.setPaused(false);
                    adapter.notifyItemChanged(adapter.getStationList().indexOf(model));
                    presenter.updateFavoriteViewModel(model);
                }
            }
        });
        sharedViewModel.getThemeViewModel().observe(getViewLifecycleOwner(), themeModel -> {
            String color = themeModel.getTheme();
            if (color.equals(Constants.THEME_RED)) {
                adapter.setStarColor(color);
            } else {
                adapter.setStarColor(color);
            }
        });
    }
}

