package infoshell.app.businessradio.fragment_favorite;

import android.annotation.SuppressLint;

import java.util.ArrayList;
import java.util.List;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.helpers.RecordingHelper;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.models.StationModel;
import infoshell.app.businessradio.models.StationModelDao;
import infoshell.app.businessradio.models.StationViewModel;
import infoshell.app.businessradio.services.PlayerService;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FavoritesFragmentPresenter implements FavoritesFragmentPresenterInterface {
    private FavoritesFragmentInterface view;
    private Disposable disposable;
    private StationModelDao stationsDao;
    private List<StationModel> favoriteStationList = new ArrayList<>();

    FavoritesFragmentPresenter(FavoritesFragmentInterface view) {
        this.view = view;
        stationsDao = App.getInstance().getAppDatabase().stationModelDao();
    }

    @Override
    public void onViewCreated() {
        loadStationsFromDatabase();
    }

    @Override
    public void setUserVisibleHint() {
        loadStationsFromDatabase();
    }

    @Override
    public void onItemClick(StationModel station) {
        if (SharedPreferencesHelper.getLastPlayedStation().isRecording) {
            PlayerService.stopRecord();
            RecordingHelper.stopRecording();
            view.changeRecordingState(false);
        }
        SharedPreferencesHelper.setSelectedStation(station);
        SharedPreferencesHelper.setLastPlayedStation(station);
        view.changeStationState(station);
        view.sendRadioMessageEventBus(String.valueOf(station.getId()), true);
    }

    @Override
    public void onItemMoved(List<StationModel> stationFavoriteList) {
        updateItems(stationFavoriteList);
    }

    @Override
    public void onFavoriteClick(StationModel station) {
        view.changeFavoriteStarColor(station);
        deleteFavoriteStationFromDB(station);
    }

    @Override
    public void updateFavoriteViewModel(StationModel station) {
        Completable.fromAction(() -> stationsDao.update(station))
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    @Override
    public void onChangedPlayerState(StationViewModel stationViewModel) {

    }

    @Override
    public void onDestroy() {
        if (disposable != null && !disposable.isDisposed()) disposable.dispose();
    }

    @SuppressLint("CheckResult")
    private void deleteFavoriteStationFromDB(StationModel station) {
        Completable.fromAction(() -> stationsDao.delete(station))
                .subscribeOn(Schedulers.io())
                .subscribe(this::loadStationsFromDatabase, Throwable::printStackTrace);

    }

    @SuppressLint("CheckResult")
    private void updateItems(List<StationModel> stationFavoriteList) {
        Completable.fromAction(() -> stationsDao.insertUpdatedStations(stationFavoriteList))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                }, Throwable::printStackTrace);

    }

    private void loadStationsFromDatabase() {
        disposable = stationsDao.getSavedStations()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stationModels -> {
                    favoriteStationList.clear();
                    favoriteStationList.addAll(stationModels);
                    view.showFavoriteStationList(stationModels);
                }, Throwable::printStackTrace);
    }
}
