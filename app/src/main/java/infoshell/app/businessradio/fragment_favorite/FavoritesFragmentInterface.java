package infoshell.app.businessradio.fragment_favorite;

import android.content.Intent;

import java.util.List;

import infoshell.app.businessradio.models.StationModel;

public interface FavoritesFragmentInterface {

    void showFavoriteStationList(List<StationModel> stationModels);

    void startService(Intent intent);

    void sendRadioMessageEventBus(String stationId, boolean isServiceStarted);

    void internetNotConnected();

    void startPlayerService();

    void pausePlayerService();

    void changeStationState(StationModel station);

    void changeRecordingState(boolean isRecording);

    void changeFavoriteStarColor(StationModel station);
}
