package infoshell.app.businessradio.fragment_station;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.helpers.RecordingHelper;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.models.FavoriteStarModel;
import infoshell.app.businessradio.models.StationModel;
import infoshell.app.businessradio.models.StationModelDao;
import infoshell.app.businessradio.models.StationViewModel;
import infoshell.app.businessradio.services.PlayerService;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class StationsFragmentPresenter implements StationsFragmentPresenterInterface {

    private StationsFragmentInterface view;
    private StationModelDao favoriteStationDao;
    private Disposable disposable;
    private SharedPreferences sharedPreferences;
    private List<StationModel> stationList = new ArrayList<>();
    private Integer clickCount = 0;

    StationsFragmentPresenter(StationsFragmentInterface view) {
        this.view = view;
        favoriteStationDao = App.getInstance().getAppDatabase().stationModelDao();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        stationList.addAll(StationsListHolder.getStationModels());
    }


    @Override
    public void onViewCreated() {
        getFavoriteStation();

    }

    @Override
    public void onUserVisibleHint() {
        stationList = new ArrayList<>(StationsListHolder.getStationModels());
        getFavoriteStation();
    }

    private void checkRecording() {
        if (SharedPreferencesHelper.getLastPlayedStation().isRecording) {
            PlayerService.stopRecord();
            RecordingHelper.stopRecording();
        }
    }

    @Override
    public void onStationClick(StationModel station) {
        clickCount++;
        checkRecording();
        SharedPreferencesHelper.setSelectedStation(station);
        SharedPreferencesHelper.setLastPlayedStation(station);
        SharedPreferencesHelper.setRecordedStation(null);
        view.changeStationState(station);
        StationsListHolder.setCurrent(station.id);
        if (clickCount % 5 == 0) {
            view.showColonyAds();
        }
    }

    @Override
    public void onFavoriteStationClick(StationModel station) {
        if (station.favorite) {
            saveStationInDB(station);
        } else {
            deleteStationInDB(station);
        }

        StationModel selectedStation = SharedPreferencesHelper.getSelectedStation();
        if (selectedStation == null) {
            createFavoriteEvent(StationsListHolder.getStationModels().get(0));
        } else if (station.getId() == selectedStation.getId()) {
            createFavoriteEvent(station);
        }
    }

    @Override
    public void onChangeFavoriteStarEvent(FavoriteStarModel favoriteStarModel) {
        view.updateFavoriteState(favoriteStarModel.getStation());
    }

    @Override
    public void onChangedPlayerState(StationViewModel stationViewModel) {

    }


    private void createFavoriteEvent(StationModel station) {
        FavoriteStarModel favoriteStarModel = new FavoriteStarModel();
        favoriteStarModel.setStation(station);
//        FavoriteStarEvent favoriteEvent = new FavoriteStarEvent();
//        favoriteEvent.setStation(station);

        view.sendFavoriteEventBus(favoriteStarModel);
    }

    private void playRadio(int stationId) {
        view.setSelectedStation(stationId);
        view.setPlaybackStatus(true);
    }

    private void stopRadio() {
        view.setPlaybackStatus(false);
    }

    @SuppressLint("CheckResult")
    private void saveStationInDB(StationModel station) {
        Completable.fromAction(() -> favoriteStationDao.insert(station))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                }, Throwable::printStackTrace);
    }

    @SuppressLint("CheckResult")
    private void deleteStationInDB(StationModel station) {
        Completable.fromAction(() -> favoriteStationDao.delete(station))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                }, Throwable::printStackTrace);
    }

    @SuppressLint("CheckResult")
    private void getFavoriteStation() {
        disposable = favoriteStationDao.getAllStations()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((stationListFavorite) -> {
                    setFavoriteStars(stationList, stationListFavorite);
                    view.showStationList(stationList);
                }, Throwable::printStackTrace);
//        Completable.fromAction(() -> favoriteStationDao.getAllStations())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(() -> {
//                    setFavoriteStars(stationList, stationListFavorite);
//                    view.showStationList(stationList);
//                }, Throwable::printStackTrace);
    }

    private void resetFavorite(List<StationModel> stationList) {
        for (StationModel item : stationList) {
            item.favorite = false;
        }
    }

    @Override
    public void onDestroy() {
        if (disposable != null && !disposable.isDisposed()) disposable.dispose();
    }

    private void setFavoriteStars(List<StationModel> stationList, List<StationModel> stationListFavorite) {
        resetFavorite(stationList);

        for (StationModel itemFavorite : stationListFavorite) {
            for (StationModel item : stationList) {
                if (itemFavorite.id == item.id && !item.favorite) {
                    item.favorite = itemFavorite.favorite;
                }
            }
        }
    }
}
