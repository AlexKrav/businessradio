package infoshell.app.businessradio.fragment_station;

import android.content.Intent;

import java.util.List;

import infoshell.app.businessradio.models.FavoriteStarModel;
import infoshell.app.businessradio.models.StationModel;

public interface StationsFragmentInterface {

    void showStationList(List<StationModel> stationModels);

    void startService(Intent intent);

    void sendFavoriteEventBus(FavoriteStarModel favoriteStarModel);

    void sendStationNameEventBus(String station);

    void setSelectedStation(int stationId);

    void setPlaybackStatus(boolean isPlayed);

    void startPlayerService();

    void updateFavoriteState(StationModel station);

    void pausePlayerService();

    void changeStationState(StationModel station);

    void showColonyAds();
}
