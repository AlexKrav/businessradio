package infoshell.app.businessradio.fragment_station;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Objects;

import infoshell.app.businessradio.R;
import infoshell.app.businessradio.adapters.FragmentStationsAdapter;
import infoshell.app.businessradio.databinding.FragmentStationsBinding;
import infoshell.app.businessradio.helpers.AdColonyControllerListener;
import infoshell.app.businessradio.helpers.DialogUtility;
import infoshell.app.businessradio.helpers.InternetConnectingManager;
import infoshell.app.businessradio.helpers.PlayerControllerListener;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.models.FavoriteStarModel;
import infoshell.app.businessradio.models.SharedViewModel;
import infoshell.app.businessradio.models.StationModel;
import infoshell.app.businessradio.models.StationNameMessageEvent;
import infoshell.app.businessradio.models.StationViewModel;

public class StationsFragment extends Fragment implements StationsFragmentInterface {

    private FragmentStationsBinding binding;
    private StationsFragmentPresenterInterface presenter;
    private FragmentStationsAdapter adapter = new FragmentStationsAdapter();
    private MutableLiveData<StationViewModel> stationModelLiveData = new MutableLiveData<>();
    private SharedViewModel sharedViewModel;
    private PlayerControllerListener playerControllerListener;
    private AdColonyControllerListener adColonyControllerListener;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter = new StationsFragmentPresenter(this);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stations, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        setupObservers();

        presenter.onViewCreated();
    }

    private void setupObservers() {
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel.class);
        sharedViewModel.getStationViewModel().observe(getViewLifecycleOwner(), stationModel -> {
            if (stationModel == null) return;
            for (StationModel model : adapter.getStationList()) {
                if (stationModel.getId() == model.getId()) {
                    model.setPlaying(stationModel.isPlaying());
                    model.setPaused(stationModel.isPaused());
                    adapter.notifyItemChanged(adapter.getStationList().indexOf(model));
                } else if (model.isPlaying() || model.isPaused()) {
                    model.setPlaying(false);
                    model.setPaused(false);
                    adapter.notifyItemChanged(adapter.getStationList().indexOf(model));
                }
            }
        });
        sharedViewModel.getRecordedStationViewModel().observe(getViewLifecycleOwner(), recordedStation -> {
//в адаптер менять состаяние
        });
        sharedViewModel.getFavoriteStarModel().observe(getViewLifecycleOwner(), favoriteStarModel -> {
            presenter.onChangeFavoriteStarEvent(favoriteStarModel);
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PlayerControllerListener) {
            playerControllerListener = (PlayerControllerListener) context;
        }
        if (context instanceof AdColonyControllerListener) {
            adColonyControllerListener = (AdColonyControllerListener) context;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
//        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (presenter != null && isVisibleToUser) {
            presenter.onUserVisibleHint();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        EventBus.getDefault().unregister(this);

    }

    @Override
    public void showStationList(List<StationModel> stationModels) {
        adapter.refill(stationModels);
    }

    @Override
    public void startService(Intent intent) {
        Objects.requireNonNull(getActivity()).startService(intent);
    }

    @Override
    public void sendFavoriteEventBus(FavoriteStarModel favoriteStarModel) {
        sharedViewModel.setFavoriteStarModel(favoriteStarModel);
//        EventBus.getDefault().post(favoriteEvent);
    }

    @Override
    public void sendStationNameEventBus(String station) {
        StationNameMessageEvent nameMessageEvent = new StationNameMessageEvent();
        nameMessageEvent.setStationName(station);
        EventBus.getDefault().post(nameMessageEvent);
    }

    @Override
    public void setSelectedStation(int stationId) {
        adapter.setSelectedStation(stationId);
    }

    @Override
    public void setPlaybackStatus(boolean isPlayed) {
        adapter.setPlaybackStatus(isPlayed);
    }

    @Override
    public void startPlayerService() {
        if (InternetConnectingManager.isInternetConnected()) {
            if (playerControllerListener != null) {
                playerControllerListener.startPlayerService();
            }
        } else internetNotConnected();

    }

    @Override
    public void pausePlayerService() {
        if (InternetConnectingManager.isInternetConnected()) {
            if (playerControllerListener != null) {
                playerControllerListener.pausePlayerService();
            }
        } else internetNotConnected();
        StationViewModel stationViewModel = new StationViewModel();
        if (SharedPreferencesHelper.getSelectedStation() == null) {
            stationViewModel.setStation(SharedPreferencesHelper.getLastPlayedStation());
        } else stationViewModel.setStation(SharedPreferencesHelper.getSelectedStation());
        stationViewModel.setPlaying(false);
        stationModelLiveData.setValue(stationViewModel);
    }

    @Override
    public void changeStationState(StationModel station) {
        StationModel previousStation = sharedViewModel.getStationViewModel().getValue();
        if (previousStation != null && station.getId() == previousStation.getId() & previousStation.isPlaying()) {
            station.setPlaying(false);
            station.setPaused(true);
        } else {

            station.setPlaying(true);
            station.setPaused(false);
        }
        if (InternetConnectingManager.isInternetConnected()) {
            if (station.isPlaying()) {
                startPlayerService();
            } else {
                pausePlayerService();
            }
        } else {
            station.setPlaying(false);
            station.setPaused(true);
            internetNotConnected();
        }
        sharedViewModel.setStationViewModel(station);
    }

    @Override
    public void updateFavoriteState(StationModel station) {
        adapter.updateFavoriteState(station);
    }


    public void internetNotConnected() {
        DialogUtility.getAlertDialog(getContext(), getResources().getString(R.string.dialog_title_text), getResources().getString(R.string.internet_not_connected), getResources().getString(R.string.dialog_btn_ok), new DialogUtility.OnAlertButtonClick() {
            @Override
            public void positiveClick(DialogInterface dialogInterface) {
                super.positiveClick(dialogInterface);
            }
        }).show();
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onChangeFavoriteStarEvent(FavoriteStarModel favoriteStarModel) {
//        presenter.onChangeFavoriteStarEvent(favoriteStarModel);
//    }

    private void initViews() {
        adapter = new FragmentStationsAdapter();
        adapter.setOnImageStarClickListener(presenter::onFavoriteStationClick);
        adapter.setOnItemClickListener(presenter::onStationClick);
        RecyclerView.ItemAnimator animator = binding.rvStations.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        binding.rvStations.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rvStations.setAdapter(adapter);
    }

    @Override
    public void showColonyAds() {
        adColonyControllerListener.showColonyAd();
    }
}
