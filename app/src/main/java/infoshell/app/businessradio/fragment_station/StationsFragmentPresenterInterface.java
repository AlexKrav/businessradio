package infoshell.app.businessradio.fragment_station;

import infoshell.app.businessradio.models.FavoriteStarModel;
import infoshell.app.businessradio.models.StationModel;
import infoshell.app.businessradio.models.StationViewModel;

public interface StationsFragmentPresenterInterface {

    void onViewCreated();

    void onUserVisibleHint();

    void onStationClick(StationModel stationModel);

    void onFavoriteStationClick(StationModel stationModel);

    void onChangeFavoriteStarEvent(FavoriteStarModel favoriteStarModel);

    void onChangedPlayerState(StationViewModel stationViewModel);

    void onDestroy();
}
