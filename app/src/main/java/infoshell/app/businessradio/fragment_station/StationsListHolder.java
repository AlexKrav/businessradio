package infoshell.app.businessradio.fragment_station;

import java.util.ArrayList;
import java.util.List;

import infoshell.app.businessradio.R;
import infoshell.app.businessradio.models.StationModel;

public class StationsListHolder {
    private static List<StationModel> stationModels;

    public static List<StationModel> getStationModels() {
        if (stationModels == null) stationModels = createList();
        return stationModels;
    }

    private static List<StationModel> createList() {

        ArrayList<StationModel> output = new ArrayList<>();
        output.add(new StationModel(1, R.drawable.business_fm_bg_3x, R.drawable.business_fm_logo_3x, false, R.string.station_business_fm, R.string.station_business_fm_url, false));
        output.add(new StationModel(2, R.drawable.exo_moskvi_bg_3x, R.drawable.exo_moskvi_logo_3x, false, R.string.station_exo_moskvi, R.string.station_exo_moskvi_url, false));
        output.add(new StationModel(3, R.drawable.commersant_fm_bg_3x, R.drawable.commersant_fm_logo_3x, false, R.string.station_commersant_fm, R.string.station_commersant_fm_url, false));
        output.add(new StationModel(4, R.drawable.radio_svoboda_bg_3x, R.drawable.radio_svoboda_logo_3x, false, R.string.station_radio_svoboda, R.string.station_radio_svoboda_url, false));
        output.add(new StationModel(5));
        output.add(new StationModel(6, R.drawable.rsn_bg_3x, R.drawable.rsn_logo_3x, false, R.string.station_rsn, R.string.station_rsn_url, false));
        output.add(new StationModel(7, R.drawable.vesti_fm_bg_3x, R.drawable.vesti_fm_logo_3x, false, R.string.station_vesti_fm, R.string.station_vesti_fm_url, false));
        output.add(new StationModel(8, R.drawable.bbc_world_service_bg_3x, R.drawable.bbc_world_service_logo_3x, false, R.string.station_bbc_world_service, R.string.station_bbc_world_service_url, false));
        output.add(new StationModel(9, R.drawable.mayak_bg_3x, R.drawable.mayak_logo_3x, false, R.string.station_mayak, R.string.station_mayak_url, false));
        output.add(new StationModel(10, R.drawable.zvezda_bg_3x, R.drawable.zvezda_logo_3x, false, R.string.station_zvezda, R.string.station_zvezda_url, false));
        output.add(new StationModel(11, R.drawable.kom_pravda_bg_3x, R.drawable.kom_pravda_logo_3x, false, R.string.station_kom_pravda, R.string.station_kom_pravda_url, false));
        output.add(new StationModel(12, R.drawable.govorit_moskva_bg_3x, R.drawable.govorit_moskva_logo_3x, false, R.string.station_govorit_moskva, R.string.station_govorit_moskva_url, false));
        output.add(new StationModel(13, R.drawable.narodnoe_radio_bg_3x, R.drawable.narodnoe_radio_logo_3x, false, R.string.station_narodnoe_radio, R.string.station_narodnoe_radio_url, false));
        return output;
    }

    private final int maxIndex = createList().size() - 1;
    private static int currentItemIndex = 0;

    public StationModel getNext() {
        if (currentItemIndex == maxIndex)
            currentItemIndex = 0;
        else
            currentItemIndex++;
        return getCurrent();
    }

    public StationModel getPrevious() {
        if (currentItemIndex == 0)
            currentItemIndex = maxIndex;
        else
            currentItemIndex--;
        return getCurrent();
    }

    public static void setCurrent(int id) {
        currentItemIndex = id - 1;
    }

    public StationModel getCurrent() {
        return createList().get(currentItemIndex);
    }
}
