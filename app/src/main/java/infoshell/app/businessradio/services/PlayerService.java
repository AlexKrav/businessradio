package infoshell.app.businessradio.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;

import infoshell.app.businessradio.R;
import infoshell.app.businessradio.fragment_station.StationsListHolder;
import infoshell.app.businessradio.helpers.ExoPlayerInstance;
import infoshell.app.businessradio.helpers.MediaStyleHelper;
import infoshell.app.businessradio.helpers.RecordingHelper;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.models.PlayingStation;
import infoshell.app.businessradio.models.RecordedStation;
import infoshell.app.businessradio.models.StationModel;

public class PlayerService extends Service {
    private boolean alarmOn = false;
    private boolean alarmOff = false;
    private final int NOTIFICATION_ID = 404;
    private final String NOTIFICATION_DEFAULT_CHANNEL_ID = "default_channel";
    private boolean isAudioFocusLossTransientCanDuck = false;
    private boolean isPlayerHasStoped = false;
    private RecordedStation recordedStation;
    private final MediaMetadataCompat.Builder metadataBuilder = new MediaMetadataCompat.Builder();

    private StationsListHolder stationsListHolder = new StationsListHolder();

    private final PlaybackStateCompat.Builder stateBuilder = new PlaybackStateCompat.Builder().setActions(
            PlaybackStateCompat.ACTION_PLAY
                    | PlaybackStateCompat.ACTION_STOP
                    | PlaybackStateCompat.ACTION_PAUSE
                    | PlaybackStateCompat.ACTION_PLAY_PAUSE
                    | PlaybackStateCompat.ACTION_SKIP_TO_NEXT
                    | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS
    );

    private MediaSessionCompat mediaSession;

    private AudioManager audioManager;
    private AudioFocusRequest audioFocusRequest;
    private boolean audioFocusRequested = false;

    private SimpleExoPlayer exoPlayer;
    private ExtractorsFactory extractorsFactory;
    private DataSource.Factory dataSourceFactory;

    private StationModel playingStation;
    private boolean isRecording = false;

    private static PlayerService instance;

    public static PlayingStation getPlayingStation() {
        if (instance != null) {
            return instance.getPlayedStation();
        } else {
            PlayingStation playingStation = new PlayingStation();
            playingStation.setPlaying(false);
            return playingStation;
        }
    }

    public static boolean isPlaying() {
        if (instance != null && instance.exoPlayer != null) {
            return instance.exoPlayer.getPlayWhenReady();
        } else {
            return false;
        }
    }


    public static boolean isRecording() {
        if (instance != null) {
            return instance.isRecording;
        } else return false;
    }

    public static boolean startRecord() {
        if (instance != null) {
            instance.isRecording = true;
            return true;
        } else {
            return false;
        }
    }

    public static boolean stopRecord() {
        if (instance != null) {
            instance.isRecording = false;
            return true;
        } else {
            return false;
        }
    }

//    private final MusicRepository musicRepository = new MusicRepository();

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_DEFAULT_CHANNEL_ID,
                    getString(R.string.notification_channel_name), NotificationManager.IMPORTANCE_LOW);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            audioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                    .setOnAudioFocusChangeListener(audioFocusChangeListener)
                    .setAcceptsDelayedFocusGain(false)
                    .setWillPauseWhenDucked(true)
                    .setAudioAttributes(audioAttributes)
                    .build();
        }

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        mediaSession = new MediaSessionCompat(this, "PlayerService");
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setCallback(mediaSessionCallback);

        Context appContext = getApplicationContext();

//        Intent activityIntent = new Intent(appContext, MainActivity.class);
//        mediaSession.setSessionActivity(PendingIntent.getActivity(appContext, 0, activityIntent, 0));

        Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON, null, appContext, MediaButtonReceiver.class);
        mediaSession.setMediaButtonReceiver(PendingIntent.getBroadcast(appContext, 0, mediaButtonIntent, 0));

        exoPlayer = ExoPlayerInstance.getExoPlayer(getApplicationContext());
        exoPlayer.addListener(exoPlayerListener);
        dataSourceFactory = new DefaultDataSourceFactory(this, "AudioPlayer");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (SharedPreferencesHelper.IsAlarmOn() != null) {
            alarmOn = true;
            mediaSessionCallback.onPlay();
        } else if (SharedPreferencesHelper.IsAlarmOff() != null) {
            alarmOff = true;
            if (SharedPreferencesHelper.getLastPlayedStation().isRecording) {
                PlayerService.stopRecord();
                RecordingHelper.stopRecording();
//                view.changeRecordingState(false);
            }
            mediaSessionCallback.onPause();
        } else MediaButtonReceiver.handleIntent(mediaSession, intent);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mediaSession.release();
        exoPlayer.release();

    }

    private MediaSessionCompat.Callback mediaSessionCallback = new MediaSessionCompat.Callback() {

        private Uri currentUri;
        int currentState = PlaybackStateCompat.STATE_STOPPED;
        String lastStationPlayed = "";

        @Override
        public void onPlay() {
            if (alarmOn) {
                playAlarm();
            } else {
                playingStation = SharedPreferencesHelper.getSelectedStation();
                recordedStation = SharedPreferencesHelper.getRecordedStation();
                if (playingStation != null) {
                    String stationUrl = getString(playingStation.url);
                    play(stationUrl);
                } else if (recordedStation != null) {
                    playRecord(recordedStation.getStoragePath());
                }
            }
        }

        private void playRecord(String storagePath) {
            prepareToPlay(Uri.parse(storagePath));
            if (!audioFocusRequested) {
                audioFocusRequested = true;

                int audioFocusResult;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    audioFocusResult = audioManager.requestAudioFocus(audioFocusRequest);
                } else {
                    audioFocusResult = audioManager.requestAudioFocus(audioFocusChangeListener,
                            AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                }
                if (audioFocusResult != AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
                    return;
            }

            mediaSession.setActive(true); // Сразу после получения фокуса

            registerReceiver(becomingNoisyReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));

            exoPlayer.setPlayWhenReady(true);

        }

        private void play(String stationUrl) {

            startService(new Intent(getApplicationContext(), PlayerService.class));
            lastStationPlayed = getString(playingStation.url);

            updateMetadataFromTrack(playingStation);
            prepareToPlay(Uri.parse(getString(playingStation.getUrl())));


            if (!audioFocusRequested) {
                audioFocusRequested = true;

                int audioFocusResult;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    audioFocusResult = audioManager.requestAudioFocus(audioFocusRequest);
                } else {
                    audioFocusResult = audioManager.requestAudioFocus(audioFocusChangeListener,
                            AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                }
                if (audioFocusResult != AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
                    return;
            }

            mediaSession.setActive(true); // Сразу после получения фокуса

            registerReceiver(becomingNoisyReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));

            exoPlayer.setPlayWhenReady(true);

            mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING,
                    PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
            currentState = PlaybackStateCompat.STATE_PLAYING;

            refreshNotificationAndForegroundStatus(currentState);
        }

        private void playAlarm() {
            StationModel stationModel = SharedPreferencesHelper.getLastPlayedStation();
            alarmOn = false;
            SharedPreferencesHelper.setAlarmOn(null);
            startService(new Intent(getApplicationContext(), PlayerService.class));

            updateMetadataFromTrack(stationModel);
            prepareToPlay(Uri.parse(getString(stationModel.getUrl())));


            if (!audioFocusRequested) {
                audioFocusRequested = true;

                int audioFocusResult;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    audioFocusResult = audioManager.requestAudioFocus(audioFocusRequest);
                } else {
                    audioFocusResult = audioManager.requestAudioFocus(audioFocusChangeListener,
                            AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                }
                if (audioFocusResult != AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
                    return;
            }

            mediaSession.setActive(true); // Сразу после получения фокуса

            registerReceiver(becomingNoisyReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));

            exoPlayer.setPlayWhenReady(true);


            mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING,
                    PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
            currentState = PlaybackStateCompat.STATE_PLAYING;

            refreshNotificationAndForegroundStatus(currentState);
        }

        @Override
        public void onPause() {
            checkRecording();
            if (alarmOff) {

                SharedPreferencesHelper.setAlarmOff(null);
                if (playingStation != null) {
                    if (exoPlayer.getPlayWhenReady()) {
                        exoPlayer.setPlayWhenReady(false);
                        unregisterReceiver(becomingNoisyReceiver);
                    }

                    mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                            PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
                    currentState = PlaybackStateCompat.STATE_PAUSED;

                    refreshNotificationAndForegroundStatus(currentState);
                } else if (recordedStation != null) {
                    if (exoPlayer.getPlayWhenReady()) {
                        exoPlayer.setPlayWhenReady(false);
                        unregisterReceiver(becomingNoisyReceiver);
                    }

                    mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                            PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
                    currentState = PlaybackStateCompat.STATE_PAUSED;
                }
            } else {

                if (playingStation != null) {
                    if (exoPlayer.getPlayWhenReady()) {
                        exoPlayer.setPlayWhenReady(false);
                        unregisterReceiver(becomingNoisyReceiver);
                    }

                    mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                            PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
                    currentState = PlaybackStateCompat.STATE_PAUSED;

                    refreshNotificationAndForegroundStatus(currentState);
                } else if (recordedStation != null) {
                    if (exoPlayer.getPlayWhenReady()) {
                        exoPlayer.setPlayWhenReady(false);
                        unregisterReceiver(becomingNoisyReceiver);
                    }

                    mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                            PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
                    currentState = PlaybackStateCompat.STATE_PAUSED;
                }
            }
        }

        @Override
        public void onStop() {
            if (exoPlayer.getPlayWhenReady()) {
                exoPlayer.setPlayWhenReady(false);
                unregisterReceiver(becomingNoisyReceiver);
            }

            if (audioFocusRequested) {
                audioFocusRequested = false;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    audioManager.abandonAudioFocusRequest(audioFocusRequest);
                } else {
                    audioManager.abandonAudioFocus(audioFocusChangeListener);
                }
            }

            mediaSession.setActive(false);

            mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_STOPPED,
                    PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
            currentState = PlaybackStateCompat.STATE_STOPPED;

            refreshNotificationAndForegroundStatus(currentState);

            stopSelf();
        }

        @Override
        public void onSkipToNext() {
            checkRecording();
            StationModel nextStation = stationsListHolder.getNext();
            if (nextStation.getId() == 5) {
                nextStation = stationsListHolder.getNext();
            }
            SharedPreferencesHelper.setLastPlayedStation(nextStation);
            SharedPreferencesHelper.setSelectedStation(nextStation);
            updateMetadataFromTrack(nextStation);
            mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_SKIPPING_TO_NEXT,
                    PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
            if (PlayerService.isPlaying()) {
                mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING,
                        PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
            } else {
                mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                        PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
            }
            refreshNotificationAndForegroundStatus(currentState);

            prepareToPlay(Uri.parse(getString(nextStation.getUrl())));
        }

        @Override
        public void onSkipToPrevious() {
            checkRecording();
            StationModel previousStation = stationsListHolder.getPrevious();
            if (previousStation.getId() == 5) {
                previousStation = stationsListHolder.getPrevious();
            }
            SharedPreferencesHelper.setLastPlayedStation(previousStation);
            SharedPreferencesHelper.setSelectedStation(previousStation);

            updateMetadataFromTrack(previousStation);
            mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_SKIPPING_TO_PREVIOUS,
                    PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
            if (PlayerService.isPlaying()) {
                mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING,
                        PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
            } else {
                mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                        PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
            }
            refreshNotificationAndForegroundStatus(currentState);

            prepareToPlay(Uri.parse(getString(previousStation.getUrl())));

        }

        private void checkRecording() {
            if (SharedPreferencesHelper.getLastPlayedStation().isRecording) {
                PlayerService.stopRecord();
                RecordingHelper.stopRecording();
            }
        }

        private void prepareToPlay(Uri uri) {
            if (!uri.equals(currentUri)) {
                currentUri = uri;
                ExtractorMediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(uri);
                exoPlayer.prepare(mediaSource);
            }
        }

        private void updateMetadataFromTrack(StationModel station) {

            metadataBuilder.putBitmap(MediaMetadataCompat.METADATA_KEY_ART, BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_web));
            metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_TITLE, getString(R.string.app_name));
            metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ALBUM, getString(station.getTitle()));
            mediaSession.setMetadata(metadataBuilder.build());
        }
    };


    private AudioManager.OnAudioFocusChangeListener audioFocusChangeListener = focusChange -> {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                if (isAudioFocusLossTransientCanDuck) {
                    exoPlayer.setVolume(1.0f);
                } else if (isPlayerHasStoped) {
                    isPlayerHasStoped = false;
                    mediaSessionCallback.onPlay();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                if (exoPlayer.getPlayWhenReady()) {
                    isPlayerHasStoped = true;
                    mediaSessionCallback.onPause();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                if (exoPlayer.getPlayWhenReady()) {
                    isAudioFocusLossTransientCanDuck = true;
                    exoPlayer.setVolume(0.3f);
                }
                break;
            default:
                mediaSessionCallback.onPause();
                break;
        }
    };

    private final BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Disconnecting headphones - stop playback
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                mediaSessionCallback.onPause();
            }
        }
    };

    private Player.EventListener exoPlayerListener = new Player.EventListener() {

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

//            if (playWhenReady && playbackState == Player.STATE_ENDED) {
//                mediaSessionCallback.onSkipToNext();
//            }
//            if (playWhenReady && playbackState == Player.STATE_ENDED) {
//                mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_STOPPED,
//                        PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
//                refreshNotificationAndForegroundStatus(PlaybackStateCompat.STATE_STOPPED);
//            }

        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new PlayerServiceBinder();
    }


    public PlayingStation getPlayedStation() {
        PlayingStation playingStation = new PlayingStation();
        playingStation.setPlaying(exoPlayer.getPlayWhenReady());
        playingStation.setStation(this.playingStation);
        return playingStation;
    }

    public class PlayerServiceBinder extends Binder {
        public MediaSessionCompat.Token getMediaSessionToken() {
            return mediaSession.getSessionToken();
        }
    }

    private void refreshNotificationAndForegroundStatus(int playbackState) {
        switch (playbackState) {
            case PlaybackStateCompat.STATE_PLAYING: {
                startForeground(NOTIFICATION_ID, getNotification(playbackState));
                break;
            }
            case PlaybackStateCompat.STATE_PAUSED: {
                NotificationManagerCompat.from(PlayerService.this).notify(NOTIFICATION_ID, getNotification(playbackState));
                stopForeground(false);
                break;
            }
            case PlaybackStateCompat.STATE_SKIPPING_TO_NEXT: {
                startForeground(NOTIFICATION_ID, getNotification(playbackState));
                break;
            }
            case PlaybackStateCompat.STATE_SKIPPING_TO_PREVIOUS: {
                startForeground(NOTIFICATION_ID, getNotification(playbackState));
                break;
            }
            case PlaybackStateCompat.STATE_STOPPED: {
//                stopForeground(false);
            }
            default: {
                stopForeground(true);
                break;
            }
        }
    }

    private Notification getNotification(int playbackState) {
        NotificationCompat.Builder builder = MediaStyleHelper.from(this, mediaSession);
        builder.addAction(new NotificationCompat.Action(R.drawable.ic_previous, getString(R.string.previous),
                MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)));

        if (playbackState == PlaybackStateCompat.STATE_PLAYING || playbackState == PlaybackStateCompat.STATE_SKIPPING_TO_NEXT || playbackState == PlaybackStateCompat.STATE_SKIPPING_TO_PREVIOUS)
            builder.addAction(new NotificationCompat.Action(R.drawable.ic_pause, getString(R.string.pause),
                    MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_PLAY_PAUSE)));
        else
            builder.addAction(new NotificationCompat.Action(R.drawable.ic_play, getString(R.string.play),
                    MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_PLAY_PAUSE)));

        builder.addAction(new NotificationCompat.Action(R.drawable.ic_next, getString(R.string.next),
                MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_SKIP_TO_NEXT)));
        builder.setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle()
                .setShowActionsInCompactView(1)
                .setShowCancelButton(true)
                .setCancelButtonIntent(MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_STOP))
                .setMediaSession(mediaSession.getSessionToken())); // setMediaSession требуется для Android Wear
        builder.setSmallIcon(R.drawable.iconnotification);
        builder.setColor(ContextCompat.getColor(this, R.color.radio_button_selected_color)); // The whole background (in MediaStyle), not just icon background
        builder.setShowWhen(false);
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setOnlyAlertOnce(true);
        builder.setChannelId(NOTIFICATION_DEFAULT_CHANNEL_ID);


        return builder.build();
    }
}