package infoshell.app.businessradio.main;

import android.content.Intent;

public interface MainActivityInterface {

    void selectStocksFragment();

    void selectRadioFragment();

    void showThemeSettingsLayout();

    void hideThemeSettingsLayout();

    void showAlarmDialog();

    void showSleepDialog();

    void setRedTheme();

    void setBlueTheme();

    void internetNotConnected();

    void showTips();

    void startSharing(Intent shareIntent);

    void setBtnPlayBackState(int playBackState);
}
