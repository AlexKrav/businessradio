package infoshell.app.businessradio.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.BaseActivity;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.adapters.FragmentPagerAdapter;
import infoshell.app.businessradio.databinding.ActivityMainBinding;
import infoshell.app.businessradio.fragment_radio.RadioFragment;
import infoshell.app.businessradio.helpers.AdColonyControllerListener;
import infoshell.app.businessradio.helpers.AdColonyHelper;
import infoshell.app.businessradio.helpers.AlarmSwitchHelper;
import infoshell.app.businessradio.helpers.BroadcastObserver;
import infoshell.app.businessradio.helpers.Constants;
import infoshell.app.businessradio.helpers.DialogUtility;
import infoshell.app.businessradio.helpers.DpToPixelConverter;
import infoshell.app.businessradio.helpers.ExoPlayerInstance;
import infoshell.app.businessradio.helpers.PlayerControllerListener;
import infoshell.app.businessradio.helpers.ProgressDialog;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.helpers.Tip;
import infoshell.app.businessradio.models.AlarmTimeEvent;
import infoshell.app.businessradio.models.SharedViewModel;
import infoshell.app.businessradio.models.StationModel;
import infoshell.app.businessradio.models.StationViewModel;
import infoshell.app.businessradio.models.ThemeModel;
import infoshell.app.businessradio.services.PlayerService;


public class MainActivity extends BaseActivity implements MainActivityInterface, RadioFragment.OnFragmentInteractionListener, PlayerControllerListener, AdColonyControllerListener {
    private ActivityMainBinding binding;
    private PagerAdapter pagerAdapter;
    private ProgressDialog progressAlarm;
    private MutableLiveData<StationViewModel> stationModelLiveData = new MutableLiveData<>();
    private SharedViewModel sharedViewModel;
    private MainActivityPresenterInterface presenter;
    private SharedPreferences.OnSharedPreferenceChangeListener spChanged;
    private ServiceConnection serviceConnection;
    private boolean isAppBarCollapsed = true;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        StationModel stationModel = sharedViewModel.getStationViewModel().getValue();
        if (stationModel == null) return;
        stationModel.setPlaying(false);
        stationModel.setPaused(false);
        EventBus.getDefault().unregister(this);
        stopPlayerService();
        App.getContext().unbindService(serviceConnection);
        presenter.onDestroy();
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main, null);
        presenter = new MainActivityPresenter(this, getBaseContext());
        configView();
        setupListeners();
        setupObservers();
        configService();
        presenter.onCreate();
    }


    @Override
    public void selectStocksFragment() {
        binding.circle1.setBackgroundResource(R.drawable.circle_unchecked);
        binding.circle2.setBackgroundResource(R.drawable.circle);
        setVisibilityForButtonLayout(true);
    }

    @Override
    public void selectRadioFragment() {
        binding.circle1.setBackgroundResource(R.drawable.circle);
        binding.circle2.setBackgroundResource(R.drawable.circle_unchecked);
        setVisibilityForButtonLayout(isAppBarCollapsed);
    }

    @Override
    public void showThemeSettingsLayout() {
        binding.settingsBlock.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideThemeSettingsLayout() {
        binding.settingsBlock.setVisibility(View.GONE);
    }

    @Override
    public void showAlarmDialog() {
        progressAlarm.show();
        progressAlarm.setVisibilityAlarm();
        progressAlarm.setAlarmSwitch();
    }

    @Override
    public void showSleepDialog() {
        progressAlarm.show();
        progressAlarm.setVisibilitySleep();
        progressAlarm.setAlarmSwitch();
    }

    @Override
    public void setRedTheme() {
        setAppThemeColor(Constants.THEME_RED);
        setTextToThemeButton(getString(R.string.theme_color_blue));
    }

    @Override
    public void setBlueTheme() {
        setAppThemeColor(Constants.THEME_BLUE);
        setTextToThemeButton(getString(R.string.theme_color_red));
    }

    @Override
    public void internetNotConnected() {
// if you need custom dialog
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
//        LayoutInflater inflater = this.getLayoutInflater();
//        View dialogView = inflater.inflate(R.layout.dialog_internet_notification, null);
//
//        Button buttonOk = dialogView.findViewById(R.id.btn_dialog_ok);
//        dialogBuilder.setView(dialogView);
//        AlertDialog alertDialog = dialogBuilder.create();
//        alertDialog.show();
//
//
//        buttonOk.setOnClickListener(v -> alertDialog.dismiss());

        DialogUtility.getAlertDialog(this, getResources().getString(R.string.dialog_title_text), getResources().getString(R.string.dialog_message), getResources().getString(R.string.dialog_btn_ok), new DialogUtility.OnAlertButtonClick() {
            @Override
            public void positiveClick(DialogInterface dialogInterface) {
                super.positiveClick(dialogInterface);
            }
        }).show();
    }

    @Override
    public void showTips() {
        Tip tips = new Tip(this);
        tips.show();
    }

    @Override
    public void startSharing(Intent shareIntent) {
        startActivity(shareIntent);
    }

    @Override
    public void setBtnPlayBackState(int playBackState) {
        StationModel stationModel = sharedViewModel.getStationViewModel().getValue();
        if (stationModel == null) return;
        StationModel station = SharedPreferencesHelper.getLastPlayedStation();

        switch (playBackState) {
            case PlaybackStateCompat.STATE_PLAYING: {
                stationModel.setPlaying(true);
                stationModel.setPaused(false);
                stationModel.setRecording(false);
//                presenter.checkRecording();
                sharedViewModel.setStationViewModel(stationModel);
                break;
            }
            case PlaybackStateCompat.STATE_PAUSED: {
                stationModel.setPlaying(false);
                stationModel.setPaused(true);
                presenter.checkRecording();
                sharedViewModel.setStationViewModel(stationModel);
                break;
            }
            case PlaybackStateCompat.STATE_SKIPPING_TO_NEXT: {
                if (PlayerService.isPlaying()) {
                    presenter.checkRecording();
                    station.setPaused(false);
                    station.setPlaying(true);
                } else {
                    station.setPaused(true);
                    station.setPlaying(false);
                }

                sharedViewModel.setStationViewModel(station);
                break;
            }
            case PlaybackStateCompat.STATE_SKIPPING_TO_PREVIOUS: {
                if (PlayerService.isPlaying()) {
                    presenter.checkRecording();
                    station.setPaused(false);
                    station.setPlaying(true);
                } else {
                    station.setPaused(true);
                    station.setPlaying(false);
                }
                sharedViewModel.setStationViewModel(station);
                break;
            }
//            case PlaybackStateCompat.STATE_STOPPED: {
//                binding.PlayerControlView.setShowTimeoutMs(0);
//                break;
//            }
        }
    }


    @Override
    public void onFragmentStart() {
        presenter.onRadioFragmentCreated();
    }

    @Override
    public void onAppBarCollapsed() {
        isAppBarCollapsed = false;
        binding.settingsBlock.setVisibility(View.GONE);
        setVisibilityForButtonLayout(false);
    }

    @Override
    public void onAppBarExpanded() {
        isAppBarCollapsed = true;
        setVisibilityForButtonLayout(true);
    }


    @Override
    public LiveData getLiveData() {
        return stationModelLiveData;
    }

    @Override
    public void startPlayerService() {
        StationViewModel stationViewModel = new StationViewModel();
        if (SharedPreferencesHelper.getSelectedStation() == null) {
            stationViewModel.setStation(SharedPreferencesHelper.getLastPlayedStation());
        } else {
            stationViewModel.setStation(SharedPreferencesHelper.getSelectedStation());
        }
        stationViewModel.setPlaying(true);
        stationModelLiveData.setValue(stationViewModel);
        presenter.startPlayerService();
        hideControlPanel();
    }

    @Override
    public void pausePlayerService() {
        presenter.pausePlayerService();
    }

    @Override
    public void stopPlayerService() {
        presenter.stopPlayerService();
    }


    private void setTextToThemeButton(String colorName) {
        binding.tvChooseColor.setText(getString(R.string.theme_format, colorName));
    }

    private void configView() {
        binding.PlayerControlView.setPlayer(ExoPlayerInstance.getExoPlayer(this));
        binding.PlayerControlView.setShowTimeoutMs(0);

        pagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager());
        binding.pager.setAdapter(pagerAdapter);
        progressAlarm = new ProgressDialog(this);

    }

    private void configService() {
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                presenter.onServiceConnected(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                presenter.onServiceDisconnected();
            }
        };

        presenter.onServiceConfig();

        App.getContext().bindService(new Intent(App.getContext(), PlayerService.class), serviceConnection, Context.BIND_AUTO_CREATE);

    }

    private void setupListeners() {

        binding.pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int pageIndex) {
                presenter.onPageSelected(pageIndex);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        binding.imgAlarm.setOnClickListener(v -> presenter.onAlarmButtonClick());

        binding.imgSleep.setOnClickListener(v -> presenter.onSleepButtonClick());

        binding.imgSettings.setOnClickListener(v ->
                presenter.onSettingsButtonClick(binding.settingsBlock.getVisibility() == View.VISIBLE));

        binding.tvChooseColor.setOnClickListener(v ->
                presenter.onChooseTheme(binding.tvChooseColor.getText().toString()));

        binding.imgShare.setOnClickListener(v -> presenter.onShareButtonClick());
        spChanged = (sharedPreferences, key) -> {
            String sharedPref = sharedPreferences.getString("ALARM_ON", null);
            if (key.equals("ALARM_ON") && sharedPref != null) {
                StationModel stationModel = SharedPreferencesHelper.getLastPlayedStation();
                AlarmTimeEvent alarmTimeEvent = AlarmTimeEvent.getAlarmTimeEventInstance();
                stationModel.setPlaying(true);
                stationModel.setPaused(false);
                sharedViewModel.setStationViewModel(stationModel);
                DialogUtility.getAlertDialog(this, getResources().getString(R.string.dialog_alarm_text), alarmTimeEvent.getDayOfWeek() + "\n" + alarmTimeEvent.getDate(), getResources().getString(R.string.dialog_btn_ok), new DialogUtility.OnAlertButtonClick() {
                    @Override
                    public void positiveClick(DialogInterface dialogInterface) {
                        super.positiveClick(dialogInterface);
                    }
                }).show();
                hideControlPanel();
                alarmTimeEvent.setWidgetName(Constants.BTN_ALARM_PRESSED);
                alarmTimeEvent.setTurnOn(false);
                alarmTimeEvent.setTime("");
                onAlarmTimeEvent(alarmTimeEvent);
                AlarmSwitchHelper.setAlarmTurnOn(false);
                progressAlarm.setAlarmSwitch();

            } else if (key.equals("ALARM_OFF")) {
                StationModel stationModel = SharedPreferencesHelper.getLastPlayedStation();
                stationModel.setPlaying(false);
                stationModel.setPaused(true);
                sharedViewModel.setStationViewModel(stationModel);
                AlarmTimeEvent alarmTimeEvent = AlarmTimeEvent.getAlarmTimeEventInstance();
                alarmTimeEvent.setWidgetName(Constants.BTN_SLEEP_PRESSED);
                alarmTimeEvent.setTurnOn(false);
                alarmTimeEvent.setTime("");
                onAlarmTimeEvent(alarmTimeEvent);
                AlarmSwitchHelper.setSleepTurnOn(false);
                progressAlarm.setAlarmSwitch();
            }
        };
        SharedPreferencesHelper.registerOnSharedPreferenceChangeListener(spChanged);
    }

    private void setupObservers() {
        sharedViewModel = ViewModelProviders.of(this).get(SharedViewModel.class);
        sharedViewModel.getRecordedStationViewModel().observe(this, recordedStation -> {
            if (recordedStation == null) return;
            if (recordedStation.isPlaying) {
                showControlPanel();
            } else {
                hideControlPanel();
            }
        });
        sharedViewModel.getAlarmViewModel().observe(this, alarmModel -> {
            if (alarmModel == null) return;
            if (alarmModel.isAlarmTurnOn()) {
                binding.tvTimeOn.setVisibility(View.VISIBLE);
                binding.tvTimeOn.setText(alarmModel.getTime());
            } else if (!alarmModel.isAlarmTurnOn()) {
                binding.tvTimeOn.setVisibility(View.INVISIBLE);
                binding.tvTimeOn.setText("");
            }
            if (alarmModel.isSleepTurnOn()) {
                binding.tvTimeOff.setVisibility(View.VISIBLE);
                binding.tvTimeOff.setText(alarmModel.getTime());
            } else if (!alarmModel.isSleepTurnOn()) {
                binding.tvTimeOff.setVisibility(View.INVISIBLE);
                binding.tvTimeOff.setText("");
            }
        });

        sharedViewModel.getThemeViewModel().observe(this, themeModel -> {

        });
        BroadcastObserver.getInstance().addObserver((o, arg) -> {

            if (PlayerService.isPlaying() && SharedPreferencesHelper.getSelectedStation() != null) {
                StationModel stationModel = sharedViewModel.getStationViewModel().getValue();
                if (stationModel == null) return;
                stationModel.setPaused(true);
                stationModel.setPlaying(false);
                sharedViewModel.setStationViewModel(stationModel);
                stopPlayerService();
            }

            DialogUtility.getAlertDialog(this, getResources().getString(R.string.dialog_title_text), getResources().getString(R.string.dialog_message), getResources().getString(R.string.dialog_btn_ok), new DialogUtility.OnAlertButtonClick() {
                @Override
                public void positiveClick(DialogInterface dialogInterface) {
                    super.positiveClick(dialogInterface);
                }
            }).show();
        });
    }

    private void setAppThemeColor(String themeColor) {
        ThemeModel themeModel = new ThemeModel();
        if (themeColor.equals(Constants.THEME_RED)) {
            themeModel.setTheme(Constants.THEME_RED);
            sharedViewModel.setThemeModel(themeModel);
        } else {
            themeModel.setTheme(Constants.THEME_BLUE);
            sharedViewModel.setThemeModel(themeModel);
        }
    }

    private void setVisibilityForButtonLayout(boolean isNeedShowButtonLayout) {
        if (isNeedShowButtonLayout) {
            binding.linearWidget.animate().alpha(1.0f);
            binding.imgAlarm.setClickable(true);
            binding.imgSleep.setClickable(true);
            binding.imgSettings.setClickable(true);
            binding.imgShare.setClickable(true);
        } else {
            binding.linearWidget.animate().alpha(0.0f);
            binding.imgAlarm.setClickable(false);
            binding.imgSleep.setClickable(false);
            binding.imgSettings.setClickable(false);
            binding.imgShare.setClickable(false);

        }
    }

    private void hideControlPanel() {
        binding.PlayerControlView.animate().translationY(DpToPixelConverter.convertDpToPixel(45, this)).start();
    }

    private void showControlPanel() {
        binding.PlayerControlView.animate().translationY(-DpToPixelConverter.convertDpToPixel(45, this)).start();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAlarmTimeEvent(AlarmTimeEvent alarmTimeEvent) {

        if (alarmTimeEvent.getWidgetName().equals(Constants.BTN_ALARM_PRESSED)) {
            if (alarmTimeEvent.isTurnOn()) {
                binding.tvTimeOn.setVisibility(View.VISIBLE);
                binding.tvTimeOn.setText(alarmTimeEvent.getTime());
            } else {
                binding.tvTimeOn.setVisibility(View.INVISIBLE);
                binding.tvTimeOn.setText(alarmTimeEvent.getTime());
            }

        } else {
            if (alarmTimeEvent.isTurnOn()) {
                binding.tvTimeOff.setVisibility(View.VISIBLE);
                binding.tvTimeOff.setText(alarmTimeEvent.getTime());
            } else {
                binding.tvTimeOff.setVisibility(View.INVISIBLE);
                binding.tvTimeOff.setText(alarmTimeEvent.getTime());
            }
        }
    }

    @Override
    public void showColonyAd() {
        AdColonyHelper.showAdColony();
    }

    @Override
    protected void onResume() {
        EventBus.getDefault().register(this);
        AdColonyHelper.initAdColony(this);
        super.onResume();
    }
}
