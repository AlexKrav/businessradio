package infoshell.app.businessradio.main;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;
import java.text.MessageFormat;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.helpers.Constants;
import infoshell.app.businessradio.helpers.InternetConnectingManager;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.models.StationModel;
import infoshell.app.businessradio.services.PlayerService;

import static infoshell.app.businessradio.helpers.Constants.STOCKS_FRAGMENT_INDEX;

public class MainActivityPresenter implements MainActivityPresenterInterface {
    private final MainActivityInterface view;

    private PlayerService.PlayerServiceBinder playerServiceBinder;
    private MediaControllerCompat mediaController;
    private MediaControllerCompat.Callback callback;

    public MainActivityPresenter(MainActivityInterface view, Context context) {
        this.view = view;
    }

    @Override
    public void onCreate() {
        if (!SharedPreferencesHelper.isFirstStart().equals("tip")) {
            SharedPreferencesHelper.setFirstAppStart("tip");
            view.showTips();
        }
        if (!InternetConnectingManager.isInternetConnected()) {
            view.internetNotConnected();
        }
    }

    @Override
    public void onDestroy() {
        playerServiceBinder = null;
        if (mediaController != null) {
            mediaController.unregisterCallback(callback);
            mediaController.getTransportControls().stop();
            mediaController = null;
        }

    }

    @Override
    public void onPageSelected(int pageIndex) {
        if (pageIndex == STOCKS_FRAGMENT_INDEX) {
            view.hideThemeSettingsLayout();
            view.selectStocksFragment();
        } else {
            view.selectRadioFragment();
        }
    }

    @Override
    public void onSettingsButtonClick(boolean isSettingsVisible) {
        if (isSettingsVisible) {
            view.hideThemeSettingsLayout();
        } else {
            view.showThemeSettingsLayout();
        }
    }

    @Override
    public void onChooseTheme(String themeName) {
        if (themeName.contains(Constants.THEME_RED) ||  themeName.contains(Constants.THEME_RED_RU)) {
            SharedPreferencesHelper.setTheme(Constants.THEME_RED);
            view.setRedTheme();
        } else {
            SharedPreferencesHelper.setTheme(Constants.THEME_BLUE);
            view.setBlueTheme();
        }
        view.hideThemeSettingsLayout();
    }

    @Override
    public void onAlarmButtonClick() {
        view.showAlarmDialog();
    }

    @Override
    public void onSleepButtonClick() {
        view.showSleepDialog();
    }

    @Override
    public void onShareButtonClick() {
        StationModel stationModel = SharedPreferencesHelper.getLastPlayedStation();
        String shareMessage = MessageFormat.format(App.getContext().getString(R.string.share_message),
                App.getContext().getString(stationModel.getTitle()),
                App.getContext().getString(R.string.app_name),
                App.getContext().getString(R.string.ios_app_link),
                App.getContext().getString(R.string.android_app_link));

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        sendIntent.setType("text/plain");

        view.startSharing(sendIntent);
    }

    @Override
    public void onRadioFragmentCreated() {
        initTheme();
    }

    @Override
    public void onServiceConfig() {
        callback = new MediaControllerCallback(this);
    }

    @Override
    public void onServiceConnected(IBinder service) {
        playerServiceBinder = (PlayerService.PlayerServiceBinder) service;
        try {
            mediaController = new MediaControllerCompat(App.getContext(), playerServiceBinder.getMediaSessionToken());
            mediaController.registerCallback(callback);
            callback.onPlaybackStateChanged(mediaController.getPlaybackState());
        } catch (RemoteException e) {
            mediaController = null;
            e.printStackTrace();
        }
    }

    @Override
    public void onServiceDisconnected() {
        playerServiceBinder = null;
        if (mediaController != null) {
            mediaController.unregisterCallback(callback);
            mediaController = null;
        }
    }

    @Override
    public void startPlayerService() {
        if (mediaController != null) {
            mediaController.getTransportControls().play();
            Boolean isRadioPlaying = true;
            EventBus.getDefault().post(isRadioPlaying);
        }
    }

    @Override
    public void pausePlayerService() {
        if (mediaController != null) {
            mediaController.getTransportControls().pause();
            Boolean isRadioPlaying = false;
            EventBus.getDefault().post(isRadioPlaying);
        }
    }

    @Override
    public void stopPlayerService() {
        if (mediaController != null) {
            mediaController.getTransportControls().stop();
            Boolean isRadioPlaying = false;
            EventBus.getDefault().post(isRadioPlaying);
        }
    }

    @Override
    public void checkRecording() {
//        if (SharedPreferencesHelper.getLastPlayedStation().isRecording) {
//            PlayerService.stopRecord();
//            RecordingHelper.stopRecording();
//        }
    }

    private void initTheme() {
        String color = SharedPreferencesHelper.getTheme();
        if (color.equals(Constants.THEME_BLUE)) {
            view.setBlueTheme();
        } else {
            view.setRedTheme();
        }
    }


    private static class MediaControllerCallback extends MediaControllerCompat.Callback {
        private WeakReference<MainActivityPresenter> weakThis;

        public MediaControllerCallback(MainActivityPresenter thiz) {
            this.weakThis = new WeakReference<>(thiz);
        }

        @Override
        public void onPlaybackStateChanged(PlaybackStateCompat state) {
            final MainActivityPresenter thiz = weakThis.get();

            if (thiz == null) {
                return;
            }

            if (state != null) {
                switch (state.getState()) {
                    case PlaybackStateCompat.STATE_PLAYING: {
                        thiz.view.setBtnPlayBackState(PlaybackStateCompat.STATE_PLAYING);
                        break;
                    }
                    case PlaybackStateCompat.STATE_PAUSED: {
                        thiz.view.setBtnPlayBackState(PlaybackStateCompat.STATE_PAUSED);
                        break;
                    }
                    case PlaybackStateCompat.STATE_SKIPPING_TO_NEXT:
                        thiz.view.setBtnPlayBackState(PlaybackStateCompat.STATE_SKIPPING_TO_NEXT);
                        break;
                    case PlaybackStateCompat.STATE_SKIPPING_TO_PREVIOUS:
                        thiz.view.setBtnPlayBackState(PlaybackStateCompat.STATE_SKIPPING_TO_PREVIOUS);
                        break;
//                    case PlaybackStateCompat.STATE_STOPPED:
//                        thiz.view.setBtnPlayBackState(PlaybackStateCompat.STATE_STOPPED);
//                        break;
                    default: {

                        break;
                    }
                }
            }
        }
    }
}
