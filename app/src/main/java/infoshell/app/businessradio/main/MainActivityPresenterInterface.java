package infoshell.app.businessradio.main;

import android.os.IBinder;

public interface MainActivityPresenterInterface {

    void onCreate();

    void onDestroy();

    void onPageSelected(int pageIndex);

    void onSettingsButtonClick(boolean isSettingsVisible);

    void onChooseTheme(String themeName);

    void onAlarmButtonClick();

    void onSleepButtonClick();

    void onShareButtonClick();

    void onRadioFragmentCreated();

    void onServiceConfig();

    void onServiceConnected(IBinder service);

    void onServiceDisconnected();

    void startPlayerService();

    void pausePlayerService();

    void stopPlayerService();

    void checkRecording();
}

