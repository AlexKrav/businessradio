package infoshell.app.businessradio.fragment_stocks;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import infoshell.app.businessradio.R;
import infoshell.app.businessradio.adapters.StocksFragmentPagerAdapter;
import infoshell.app.businessradio.databinding.FragmentStocksBinding;

import static infoshell.app.businessradio.helpers.Constants.CURRENCIES_FRAGMENT_INDEX;

public class StocksFragment extends Fragment implements StocksFragmentInterface, RadioGroup.OnCheckedChangeListener {

    private FragmentStocksBinding binding;
    private StocksFragmentPresenterInterface presenter;

    private SparseIntArray fragmentMap;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stocks, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new StocksFragmentPresenter(this);

        binding.pagerFragmentStocks.setAdapter(new StocksFragmentPagerAdapter(getFragmentManager()));
        binding.pagerFragmentStocks.setSwipeable(false);

        binding.segmentedGroup.check(binding.currenciesBtn.getId());
        binding.segmentedGroup.setOnCheckedChangeListener(this);

        initFragmentMap();

        presenter.onViewCreated();
    }

    @Override
    public void showCurrentDay(String date) {
        binding.dateTV.setText(date);
    }

    @Override
    public void showCurrenciesFragment() {
        binding.segmentedGroup.check(binding.currenciesBtn.getId());
        binding.pagerFragmentStocks.setCurrentItem(0);
    }

    @Override
    public void showIndicesFragment() {
        // add for INDICES_FRAGMENT
//        binding.segmentedGroup.check(binding.indicesBtn.getId());
//        binding.pagerFragmentStocks.setCurrentItem(1);
    }

    @Override
    public void showCommoditiesFragment() {
        // add for COMMODITIES_FRAGMENT
//        binding.segmentedGroup.check(binding.commoditiesBtn.getId());
//        binding.pagerFragmentStocks.setCurrentItem(2);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        presenter.onCheckedChanged(fragmentMap.get(checkedId));
    }

    private void initFragmentMap() {
        fragmentMap = new SparseIntArray();
        fragmentMap.put(binding.currenciesBtn.getId(), CURRENCIES_FRAGMENT_INDEX);
        // add for INDICES_FRAGMENT and COMMODITIES_FRAGMENT
//        fragmentMap.put(binding.indicesBtn.getId(), INDICES_FRAGMENT_INDEX);
//        fragmentMap.put(binding.commoditiesBtn.getId(), COMMODITIES_FRAGMENT_INDEX);
    }
}
