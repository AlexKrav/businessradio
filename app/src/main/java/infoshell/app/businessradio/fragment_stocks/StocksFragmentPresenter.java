package infoshell.app.businessradio.fragment_stocks;

import java.util.Calendar;
import java.util.Locale;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.helpers.DateFormater;

import static infoshell.app.businessradio.helpers.Constants.CURRENCIES_FRAGMENT_INDEX;

public class StocksFragmentPresenter implements StocksFragmentPresenterInterface {
    StocksFragmentInterface view;

    public StocksFragmentPresenter(StocksFragmentInterface view) {
        this.view = view;
    }


    @Override
    public void onViewCreated() {
        String language = Locale.getDefault().getDisplayLanguage();
        Locale currentLocale = App.getContext().getResources().getConfiguration().locale;
        if (language.equals("русский")) {
            view.showCurrentDay(DateFormater.getDate(Calendar.getInstance(), currentLocale));
        } else view.showCurrentDay(DateFormater.getDate(Calendar.getInstance(), Locale.ENGLISH));

        view.showCurrenciesFragment();
    }

    @Override
    public void onCheckedChanged(int checkedId) {
        switch (checkedId) {
            case CURRENCIES_FRAGMENT_INDEX:
                view.showCurrenciesFragment();
                break;
            // add for INDICES_FRAGMENT and COMMODITIES_FRAGMENT
//            case INDICES_FRAGMENT_INDEX:
//                view.showIndicesFragment();
//                break;
//            case COMMODITIES_FRAGMENT_INDEX:
//                view.showCommoditiesFragment();
//                break;
            default:
                view.showCurrenciesFragment();
                break;
        }
    }

}
