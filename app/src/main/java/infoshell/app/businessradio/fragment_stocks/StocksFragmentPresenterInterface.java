package infoshell.app.businessradio.fragment_stocks;

public interface StocksFragmentPresenterInterface {

    void onViewCreated();

    void onCheckedChanged(int checkedId);
}
