package infoshell.app.businessradio.fragment_stocks;

public interface StocksFragmentInterface {

    void showCurrentDay(String date);

    void showCurrenciesFragment();

    void showIndicesFragment();

    void showCommoditiesFragment();
}
