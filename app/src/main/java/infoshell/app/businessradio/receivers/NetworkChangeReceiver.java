package infoshell.app.businessradio.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import infoshell.app.businessradio.R;
import infoshell.app.businessradio.helpers.BroadcastObserver;
import infoshell.app.businessradio.helpers.DialogUtility;
import infoshell.app.businessradio.helpers.NetworkUtil;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {

        int status = NetworkUtil.getConnectivityStatusString(context);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if (status == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
                BroadcastObserver.getInstance().updateValue(intent);
                Log.e("TAG", "Not connected");
            } else {

            }
        }
    }
}
