package infoshell.app.businessradio.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.helpers.Constants;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.services.PlayerService;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
//        Toast.makeText(context, "AlarmReceiver onReceive", Toast.LENGTH_SHORT).show();

        //this will sound the alarm tone
        //this will sound the alarm once, if you wish to
        //raise alarm in loop continuously then use MediaPlayer and setLooping(true)
//        Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
//        if (alarmUri == null) {
//            alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        }
//        Ringtone ringtone = RingtoneManager.getRingtone(context, alarmUri);
//        ringtone.play();

//        PlayingStation playingStation = PlayerService.getPlayingStation();
//        if (!playingStation.isPlaying()) {
//
//        } else Toast.makeText(context, "Radio is already playing", Toast.LENGTH_SHORT).show();
        if (App.isAppOnForeground(context)) {
            String s = intent.getStringExtra("alarm");
            if (s.equals(Constants.BTN_ALARM_PRESSED)) {
                SharedPreferencesHelper.setAlarmOn("alarmOn");
            } else if (s.equals(Constants.BTN_SLEEP_PRESSED)) {
                SharedPreferencesHelper.setAlarmOff("alarmOff");
            }

            Intent intent1 = new Intent(context, PlayerService.class);
            intent.putExtra("last", "someShit");
            context.startService(intent1);
        }
    }
}
