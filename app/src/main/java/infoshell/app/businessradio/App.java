package infoshell.app.businessradio;

import android.app.ActivityManager;
import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import java.util.List;

import infoshell.app.businessradio.database.AppDatabase;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends Application {

    private static App instance;
    private AppDatabase appDatabase;

    public static App getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance;
    }

    public AppDatabase getAppDatabase() {
        return appDatabase;
    }
    public static boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.robo_regular))
                .setFontAttrId(R.attr.fontPath)
                .build());
        appDatabase = Room.databaseBuilder(this, AppDatabase.class, "appDatabase")
                .fallbackToDestructiveMigration()
                .build();
    }
}
