package infoshell.app.businessradio.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import infoshell.app.businessradio.models.RecordedStation;
import infoshell.app.businessradio.models.RecordedStationModelDao;
import infoshell.app.businessradio.models.StationModel;
import infoshell.app.businessradio.models.StationModelDao;

@Database(entities = {StationModel.class, RecordedStation.class}, version = 4)
public abstract class AppDatabase extends RoomDatabase {

    public abstract StationModelDao stationModelDao();

    public abstract RecordedStationModelDao recordedStationModelDao();

}
