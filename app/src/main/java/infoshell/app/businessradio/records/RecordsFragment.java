package infoshell.app.businessradio.records;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.List;

import infoshell.app.businessradio.R;
import infoshell.app.businessradio.adapters.RecordedStationAdapter;
import infoshell.app.businessradio.databinding.FragmentRecordsBinding;
import infoshell.app.businessradio.helpers.PlayerControllerListener;
import infoshell.app.businessradio.helpers.RecyclerItemTouchHelper;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.models.RecordedStation;
import infoshell.app.businessradio.models.SharedViewModel;
import infoshell.app.businessradio.models.StationModel;
import infoshell.app.businessradio.services.PlayerService;

public class RecordsFragment extends Fragment implements RecordsFragmentInterface {
    private FragmentRecordsBinding binding;
    private RecordsFragmentPresenterInterface presenter;
    private RecordedStationAdapter adapter;
    private PlayerControllerListener playerControllerListener;
    private SharedViewModel sharedViewModel;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PlayerControllerListener) {
            playerControllerListener = (PlayerControllerListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter = new RecordsFragmentPresenter(this);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_records, container, false);
        configView();
        configAdapter();
        return binding.getRoot();
    }

    private void configAdapter() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rvRecordedStations.setLayoutManager(llm);
        binding.rvRecordedStations.setItemAnimator(new DefaultItemAnimator());
        binding.rvRecordedStations.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        RecyclerView.ItemAnimator animator = binding.rvRecordedStations.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void configView() {
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel.class);
        sharedViewModel.getRecordedStationViewModel().observe(getViewLifecycleOwner(), recordedStation -> {
            if (recordedStation == null) return;
            for (RecordedStation model : adapter.getItems()) {
                if (recordedStation.getId() == model.getId()) {
                    model.setPlaying(recordedStation.isPlaying());
                    model.setPaused(recordedStation.isPaused());
                    adapter.notifyItemChanged(adapter.getItems().indexOf(model));
                } else if (model.isPlaying() || model.isPaused()) {
                    model.setPlaying(false);
                    model.setPaused(false);
                    adapter.notifyItemChanged(adapter.getItems().indexOf(model));
                }
            }
        });
        sharedViewModel.getStationViewModel().observe(getViewLifecycleOwner(), stationModel -> {
            if (stationModel == null || adapter == null) return;

            for (RecordedStation model : adapter.getItems()) {
                if (model.isPlaying()) {
                    model.setPlaying(false);
                    model.setPaused(false);
                    adapter.notifyItemChanged(adapter.getItems().indexOf(model));
                }
            }
        });

        binding.tvNoRecords.setVisibility(View.GONE);
        binding.rvRecordedStations.setVisibility(View.VISIBLE);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.viewCreated();
    }


    @Override
    public void showRecordedStationList(List<RecordedStation> recordedStationList) {

        if (recordedStationList.size() > 0) {
            binding.tvNoRecords.setVisibility(View.GONE);
            adapter = new RecordedStationAdapter(recordedStationList);
            binding.rvRecordedStations.setAdapter(adapter);

            ItemTouchHelper.SimpleCallback itemTouchHelperItemCallBack = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, (viewHolder, direction, position) -> {
                if (viewHolder instanceof RecordedStationAdapter.RecordedStationHolder) {
                    int swipedPosition = viewHolder.getAdapterPosition();
                    RecordedStation recordedStation = adapter.getItem(swipedPosition);
                    adapter.remove(swipedPosition);
                    presenter.removeRecord(recordedStation);
                    if (PlayerService.isPlaying() && SharedPreferencesHelper.getSelectedStation() != null) {
                        return;
                    }
                    playerControllerListener.stopPlayerService();
                    recordedStation.setPlaying(false);
                    sharedViewModel.setRecordedStation(recordedStation);
                }
            });
            new ItemTouchHelper(itemTouchHelperItemCallBack).attachToRecyclerView(binding.rvRecordedStations);
            adapter.setOnPlayClickedListener(recordedStation -> {
                presenter.onPlayBtnClick(recordedStation);
            });
            adapter.setOnDeleteClickedListener((recordedStation, i) -> {
                adapter.remove(i);
                presenter.removeRecord(recordedStation);
                if (PlayerService.isPlaying() && SharedPreferencesHelper.getSelectedStation() != null) {
                    return;
                }
                playerControllerListener.stopPlayerService();
                recordedStation.setPlaying(false);
                sharedViewModel.setRecordedStation(recordedStation);
            });

            adapter.setOnItemTitleChangeListener(presenter::setNewTitle);

            adapter.setOnScrollRvToPosition((position -> binding.rvRecordedStations.scrollToPosition(position)));

            adapter.setOnOpenKeyboardListener(new RecordedStationAdapter.OnOpenKeyboardListener() {
                InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

                @Override
                public void onOpenKeyboard(EditText tvTitle) {
                    inputMethodManager.showSoftInput(tvTitle, 0);
                }

                @Override
                public void onHideKeyBoard(EditText tvTitle) {
                    inputMethodManager.hideSoftInputFromWindow(tvTitle.getWindowToken(), 0);
                }
            });
        } else {
            binding.tvNoRecords.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void changeStationState(RecordedStation recordedStation) {
        RecordedStation previousStation = sharedViewModel.getRecordedStationViewModel().getValue();
        StationModel stationModel = sharedViewModel.getStationViewModel().getValue();
        if (previousStation != null && recordedStation.getId() == previousStation.getId() & previousStation.isPlaying()) {
            recordedStation.setPlaying(false);
            recordedStation.setPaused(true);
        } else {
            if (stationModel != null) {
                stationModel.setPlaying(false);
                stationModel.setPaused(true);
                sharedViewModel.setStationViewModel(stationModel);
            }
            recordedStation.setPlaying(true);
            recordedStation.setPaused(false);
        }

        if (recordedStation.isPlaying()) {
            playerControllerListener.stopPlayerService();
            playerControllerListener.startPlayerService();
        } else {
            playerControllerListener.stopPlayerService();
        }
        sharedViewModel.setRecordedStation(recordedStation);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.destroyed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
