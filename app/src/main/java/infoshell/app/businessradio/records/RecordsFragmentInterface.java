package infoshell.app.businessradio.records;

import java.util.List;

import infoshell.app.businessradio.models.RecordedStation;

public interface RecordsFragmentInterface {
    void showRecordedStationList(List<RecordedStation> recordedStationList);

    void changeStationState(RecordedStation recordedStation);

//    void changeRecordingState(boolean isRecording);
}
