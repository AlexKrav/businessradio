package infoshell.app.businessradio.records;

import android.annotation.SuppressLint;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.helpers.RecordingHelper;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.models.RecordedStation;
import infoshell.app.businessradio.models.RecordedStationModelDao;
import infoshell.app.businessradio.models.SharedViewModel;
import infoshell.app.businessradio.services.PlayerService;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RecordsFragmentPresenter implements RecordsFragmentPresenterInterface {
    private RecordsFragmentInterface view;
    private RecordedStationModelDao recordedStationModelDao;
    private Disposable disposable;
    private List<RecordedStation> recordedStationsList = new ArrayList<>();

    RecordsFragmentPresenter(RecordsFragmentInterface view) {
        this.view = view;
        recordedStationModelDao = App.getInstance().getAppDatabase().recordedStationModelDao();
    }

    @Override
    public void viewCreated() {
        getRecordedStations();
    }

    @SuppressLint("CheckResult")
    @Override
    public void removeRecord(RecordedStation recordedStation) {
        File file = new File(recordedStation.getStoragePath());
        if (file.exists()) {
            if (file.delete()) {
                Log.d("deleted", "file deleted");
            }
        }

        Completable.fromAction(() -> recordedStationModelDao.delete(recordedStation))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                }, Throwable::printStackTrace);
    }

    @SuppressLint("CheckResult")
    @Override
    public void setNewTitle(RecordedStation recordedStation) {
        Completable.fromAction(() -> recordedStationModelDao.update(recordedStation))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                }, Throwable::printStackTrace);
    }

    @Override
    public void onPlayBtnClick(RecordedStation recordedStation) {
        if (SharedPreferencesHelper.getLastPlayedStation().isRecording) {
            PlayerService.stopRecord();
            RecordingHelper.stopRecording();
        }
        SharedPreferencesHelper.setRecordedStation(recordedStation);
        SharedPreferencesHelper.setSelectedStation(null);
        view.changeStationState(recordedStation);
    }

    @Override
    public void updateRecordedViewModel(RecordedStation recordedStation) {
        Completable.fromAction(() -> recordedStationModelDao.update(recordedStation))
                .subscribeOn(Schedulers.io())
                .subscribe();
    }


    private void getRecordedStations() {
        disposable = recordedStationModelDao.getAllRecordedStations()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recordedStationList -> {
                    recordedStationsList.clear();
                    recordedStationsList.addAll(recordedStationList);
                    RecordedStation station = SharedViewModel.recordedStation;
                    if (station != null && station.isPlaying) {
                        for (RecordedStation recordedStation : recordedStationList) {
                            if (recordedStation.getId() == station.id) {
                                recordedStation.setPlaying(true);
                                recordedStation.setPaused(false);
                            }
                        }
                    }
                    view.showRecordedStationList(recordedStationList);
                }, Throwable::printStackTrace);
    }


    @Override
    public void destroyed() {
        if (disposable != null && !disposable.isDisposed()) disposable.isDisposed();
    }


}
