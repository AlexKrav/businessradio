package infoshell.app.businessradio.records;

import infoshell.app.businessradio.models.RecordedStation;
import infoshell.app.businessradio.models.StationViewModel;

public interface RecordsFragmentPresenterInterface {

    void destroyed();

    void viewCreated();

    void removeRecord(RecordedStation recordedStation);

    void setNewTitle(RecordedStation recordedStation);

    void onPlayBtnClick(RecordedStation recordedStation);

    void updateRecordedViewModel(RecordedStation model);
}
