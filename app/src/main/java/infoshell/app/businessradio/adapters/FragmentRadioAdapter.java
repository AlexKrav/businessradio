package infoshell.app.businessradio.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.fragment_favorite.FavoritesFragment;
import infoshell.app.businessradio.records.RecordsFragment;
import infoshell.app.businessradio.fragment_station.StationsFragment;

public class FragmentRadioAdapter extends android.support.v4.app.FragmentPagerAdapter {
    private String[] tabs;

    public FragmentRadioAdapter(FragmentManager fm) {
        super(fm);
        tabs = new String[]{
                App.getContext().getResources().getString(R.string.radio),
                App.getContext().getResources().getString(R.string.favorites),
                App.getContext().getResources().getString(R.string.my_records)
        };
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new StationsFragment();
            case 1:
                return new FavoritesFragment();
            case 2:
                return new RecordsFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabs.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }
}
