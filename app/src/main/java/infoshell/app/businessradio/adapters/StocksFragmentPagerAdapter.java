package infoshell.app.businessradio.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import infoshell.app.businessradio.fragment_commodities.CommoditiesFragment;
import infoshell.app.businessradio.fragment_currencies.CurrenciesFragment;
import infoshell.app.businessradio.fragment_indices.IndicesFragment;

import static infoshell.app.businessradio.helpers.Constants.COMMODITIES_FRAGMENT_INDEX;
import static infoshell.app.businessradio.helpers.Constants.CURRENCIES_FRAGMENT_INDEX;
import static infoshell.app.businessradio.helpers.Constants.INDICES_FRAGMENT_INDEX;
import static infoshell.app.businessradio.helpers.Constants.STOCKS_FRAGMENT_COUNT;

public class StocksFragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

    public StocksFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {

        switch (i) {
            case CURRENCIES_FRAGMENT_INDEX:
                return new CurrenciesFragment();
            case INDICES_FRAGMENT_INDEX:
                return new IndicesFragment();
            case COMMODITIES_FRAGMENT_INDEX:
                return new CommoditiesFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return STOCKS_FRAGMENT_COUNT;
    }
}
