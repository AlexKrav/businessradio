package infoshell.app.businessradio.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.databinding.RecordedStationItemBinding;
import infoshell.app.businessradio.models.RecordedStation;

public class RecordedStationAdapter extends BaseAdapter<RecordedStation, RecordedStationAdapter.RecordedStationHolder> {
    private boolean isEditing = true;
    private Context context;
    private OnItemTitleChangeListener onItemTitleChangeListener;
    private OnPlayClickedListener onPlayClickedListener;
    private OnDeleteClickedListener onDeleteClickedListener;
    private OnScrollRvToPosition onScrollRvToPosition;
    private OnOpenKeyboardListener onOpenKeyboardListener;
    private String oldTitle;
    private RecordedStation previousItem;
    private RecordedStationHolder previousItemHolder;

    public void setOnOpenKeyboardListener(OnOpenKeyboardListener onOpenKeyboardListener) {
        this.onOpenKeyboardListener = onOpenKeyboardListener;
    }

    public void setOnItemTitleChangeListener(OnItemTitleChangeListener onItemTitleChangeListener) {
        this.onItemTitleChangeListener = onItemTitleChangeListener;
    }

    public void setOnPlayClickedListener(OnPlayClickedListener onPlayClickedListener) {
        this.onPlayClickedListener = onPlayClickedListener;
    }

    public void setOnDeleteClickedListener(OnDeleteClickedListener onDeleteClickedListener) {
        this.onDeleteClickedListener = onDeleteClickedListener;
    }

    public void setOnScrollRvToPosition(OnScrollRvToPosition onScrollRvToPosition) {
        this.onScrollRvToPosition = onScrollRvToPosition;
    }

    public RecordedStationAdapter(List<RecordedStation> items) {
        super(items);
    }

    @NonNull
    @Override
    public RecordedStationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        RecordedStationItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recorded_station_item, viewGroup, false);
        context = viewGroup.getContext();
        return new RecordedStationHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecordedStationHolder recordedStationHolder, int i) {
        RecordedStation station = getItem(i);
        recordedStationHolder.binding.setRecordedStation(station);
        String s = App.getContext().getResources().getString(station.title);
        if (station.changedTitle != null && !station.changedTitle.equals(s)) {
            recordedStationHolder.binding.tvTitle.setText(station.changedTitle);
        } else recordedStationHolder.binding.tvTitle.setText(s);
        setTime(station, recordedStationHolder);

        if (station.isPlaying()) {
            recordedStationHolder.binding.tvPlay.setText(App.getContext().getResources().getString(R.string.pause));
            recordedStationHolder.binding.tvPlay.setTextColor(App.getContext().getResources().getColor(R.color.MyRed));
            recordedStationHolder.binding.viewForeground.setBackgroundColor(App.getContext().getResources().getColor(R.color.favorite_cheked_item));
        } else {

            recordedStationHolder.binding.tvPlay.setText(App.getContext().getResources().getString(R.string.play));
            recordedStationHolder.binding.tvPlay.setTextColor(App.getContext().getResources().getColor(R.color.tv_record_play));
            recordedStationHolder.binding.viewForeground.setBackgroundColor(App.getContext().getResources().getColor(R.color.colorBlack));

        }

        recordedStationHolder.binding.ivDelete.setOnClickListener(v -> {
            onDeleteClickedListener.onDeleteClicked(station, i);
        });
        recordedStationHolder.binding.tvPlay.setOnClickListener(v -> {
            onPlayClickedListener.onPlayClicked(station);
        });
        recordedStationHolder.binding.tvTitle.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                setNewTitle(recordedStationHolder, i, station);
                return true;
            }
            return false;
        });
        recordedStationHolder.binding.ivEditTitle.setOnClickListener(view -> {
            setNewTitle(recordedStationHolder, i, station);
        });
    }

    @SuppressLint("SetTextI18n")
    private void setTime(RecordedStation station, RecordedStationHolder recordedStationHolder) {
        String dateFormat = "dd MMM yyyy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(station.date);
        recordedStationHolder.binding.tvDate.setText(dateFormatter.format(calendar.getTime()) + " " + context.getResources().getString(R.string.in) + " " + android.text.format.DateFormat.getTimeFormat(context).format(calendar.getTime()));
    }

    private void setNewTitle(@NonNull RecordedStationHolder recordedStationHolder, int i, RecordedStation station) {
        int length = recordedStationHolder.binding.tvTitle.getText().length();
        recordedStationHolder.binding.tvTitle.setEnabled(true);
        recordedStationHolder.binding.tvTitle.requestFocus();
        recordedStationHolder.binding.tvTitle.setSelection(length);
        setItemParams(recordedStationHolder, i);
        if (isEditing) {
            if (previousItem.getId() == station.getId()) {
                String newTitle = recordedStationHolder.binding.tvTitle.getText().toString();
                recordedStationHolder.binding.tvTitle.setEnabled(false);
                if (!newTitle.equals(oldTitle)) {
                    station.changedTitle = newTitle;
                    onItemTitleChangeListener.onItemTitleChange(station);
                }
                recordedStationHolder.binding.tvTitle.setText(newTitle);
            } else {
                previousItemHolder.binding.ivEditTitle.setImageResource(R.drawable.button_edit_3x);
                previousItemHolder.binding.tvTitle.setEnabled(false);
                setItemParams(recordedStationHolder, i);
                saveOldViewParams(recordedStationHolder.binding.tvTitle.getText().toString(), recordedStationHolder, station);
            }

        } else {
            saveOldViewParams(recordedStationHolder.binding.tvTitle.getText().toString(), recordedStationHolder, station);

        }
    }

    private void saveOldViewParams(String s, RecordedStationHolder recordedStationHolder, RecordedStation station) {
        oldTitle = s;
        previousItemHolder = recordedStationHolder;
        previousItem = station;
    }

    private void setItemParams(RecordedStationHolder recordedStationHolder, int position) {

        if (isEditing) {
            recordedStationHolder.binding.ivEditTitle.setImageResource(R.drawable.button_edit_hl_3x);
            onOpenKeyboardListener.onOpenKeyboard(recordedStationHolder.binding.tvTitle);
            onScrollRvToPosition.onScrollToPosition(position);
            isEditing = false;
        } else {
            recordedStationHolder.binding.ivEditTitle.setImageResource(R.drawable.button_edit_3x);
            onOpenKeyboardListener.onHideKeyBoard(recordedStationHolder.binding.tvTitle);
            isEditing = true;
        }
    }

    public class RecordedStationHolder extends RecyclerView.ViewHolder {
        public RecordedStationItemBinding binding;

        RecordedStationHolder(RecordedStationItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface OnItemTitleChangeListener {
        void onItemTitleChange(RecordedStation station);
    }

    public interface OnPlayClickedListener {
        void onPlayClicked(RecordedStation recordedStation);
    }

    public interface OnDeleteClickedListener {
        void onDeleteClicked(RecordedStation recordedStation, int i);
    }

    public interface OnScrollRvToPosition {
        void onScrollToPosition(int position);
    }

    public interface OnOpenKeyboardListener {
        void onOpenKeyboard(EditText tvTitle);

        void onHideKeyBoard(EditText tvTitle);
    }
}
