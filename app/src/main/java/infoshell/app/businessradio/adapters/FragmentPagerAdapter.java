package infoshell.app.businessradio.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import infoshell.app.businessradio.fragment_radio.RadioFragment;
import infoshell.app.businessradio.fragment_stocks.StocksFragment;

import static infoshell.app.businessradio.helpers.Constants.RADIO_FRAGMENT_INDEX;
import static infoshell.app.businessradio.helpers.Constants.STOCKS_FRAGMENT_INDEX;

public class FragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

    public FragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {

        switch (i) {
            case RADIO_FRAGMENT_INDEX:
                return new RadioFragment();
            case STOCKS_FRAGMENT_INDEX:
                return new StocksFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
