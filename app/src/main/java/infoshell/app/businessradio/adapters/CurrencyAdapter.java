package infoshell.app.businessradio.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.databinding.CurrencyItemBinding;
import infoshell.app.businessradio.models.CurrencyViewModel;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyHolder> {
    private List<CurrencyViewModel> currencyList = new ArrayList<>();

    public void setCurrencyList(List<CurrencyViewModel> currencyList) {
        this.currencyList = currencyList;

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CurrencyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        CurrencyItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.currency_item, viewGroup, false);
        return new CurrencyHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyHolder currencyStationsHolder, int i) {

        currencyStationsHolder.currencyItemBinding.title.setText(currencyList.get(i).getTitle());

        if (currencyList.get(i).getDifference() > 0) {
            currencyStationsHolder.currencyItemBinding.value.setTextColor(App.getContext().getResources().getColor(R.color.currency_positive));
        } else {
            currencyStationsHolder.currencyItemBinding.value.setTextColor(App.getContext().getResources().getColor(R.color.currency_negative));

        }
        currencyStationsHolder.currencyItemBinding.value.setText(String.format(Locale.ENGLISH,
                App.getContext().getString(R.string.currency_format), currencyList.get(i).getValue()));


        currencyStationsHolder.currencyItemBinding.percent.setText(String.format(Locale.ENGLISH,
                App.getContext().getString(R.string.currency_percent_sign), currencyList.get(i).getPercent()));

        currencyStationsHolder.currencyItemBinding.difference.setText(String.format(Locale.ENGLISH,
                App.getContext().getString(R.string.currency_format_sign), currencyList.get(i).getDifference()));

    }

    @Override
    public int getItemCount() {
        return currencyList.size();
    }

    class CurrencyHolder extends RecyclerView.ViewHolder {
        CurrencyItemBinding currencyItemBinding;

        CurrencyHolder(CurrencyItemBinding currencyItemBinding) {
            super(currencyItemBinding.getRoot());
            this.currencyItemBinding = currencyItemBinding;
        }
    }

}
