package infoshell.app.businessradio.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.List;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.databinding.StationItemBinding;
import infoshell.app.businessradio.models.StationModel;

public class FragmentStationsAdapter extends RecyclerView.Adapter<FragmentStationsAdapter.FragmentStationsHolder> {
    private List<StationModel> stationList;
    private OnItemClickListener onItemClickListener;
    private OnImageStarClickListener onImageStarClickListener;
    private boolean onAir = true;
    private int selectedStationId = -1;

    public FragmentStationsAdapter() {
        this.stationList = new ArrayList<>();
    }

    public void setOnImageStarClickListener(OnImageStarClickListener onImageStarClickListener) {
        this.onImageStarClickListener = onImageStarClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void refill(List<StationModel> stationList) {
        this.stationList.clear();
        this.stationList.addAll(stationList);
        notifyDataSetChanged();
    }

    public List<StationModel> getStationList() {
        return stationList;
    }

    public void setSelectedStation(int stationId) {
        selectedStationId = stationId;
    }

    public void setPlaybackStatus(boolean isPlayed) {
        onAir = isPlayed;
    }

    @Override
    public int getItemCount() {
        return stationList.size();
    }

    @NonNull
    @Override
    public FragmentStationsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        StationItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.station_item, viewGroup, false);

        return new FragmentStationsHolder(binding);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull FragmentStationsHolder holder) {
        holder.setIsRecyclable(false);
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull FragmentStationsHolder holder) {
        holder.setIsRecyclable(false);
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public void onBindViewHolder(@NonNull FragmentStationsHolder fragmentStationsHolder, int i) {
        StationModel station = stationList.get(i);
        if (station.getId() == 5) {
            MobileAds.initialize(App.getContext(), App.getContext().getString(R.string.admob_app_id));
            AdRequest adRequest = new AdRequest.Builder().build();
            fragmentStationsHolder.stationItemBinding.adView.loadAd(adRequest);

            fragmentStationsHolder.stationItemBinding.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    fragmentStationsHolder.stationItemBinding.radioItem.setVisibility(View.VISIBLE);
                    fragmentStationsHolder.stationItemBinding.adView.setVisibility(View.VISIBLE);
                    setAdsItemMargin(fragmentStationsHolder, true);
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    fragmentStationsHolder.stationItemBinding.radioItem.setVisibility(View.GONE);
                    fragmentStationsHolder.stationItemBinding.adView.setVisibility(View.GONE);
                    setAdsItemMargin(fragmentStationsHolder, false);
                }
            });

            fragmentStationsHolder.stationItemBinding.ivStationPlay.setVisibility(View.GONE);
            fragmentStationsHolder.stationItemBinding.ivStationItemBackground.setVisibility(View.GONE);
            fragmentStationsHolder.stationItemBinding.ivStationItemLogo.setVisibility(View.GONE);
            fragmentStationsHolder.stationItemBinding.ivStationFavorite.setVisibility(View.GONE);
            fragmentStationsHolder.stationItemBinding.radioItem.setClickable(false);
            return;
        }
        setItemMargin(fragmentStationsHolder);
        setItemVisibility(fragmentStationsHolder, View.VISIBLE);
        fragmentStationsHolder.stationItemBinding.adView.setVisibility(View.INVISIBLE);
        fragmentStationsHolder.stationItemBinding.ivStationPlay.setVisibility(View.VISIBLE);
        fragmentStationsHolder.stationItemBinding.ivStationItemBackground.setVisibility(View.VISIBLE);
        fragmentStationsHolder.stationItemBinding.ivStationItemLogo.setVisibility(View.VISIBLE);
        fragmentStationsHolder.stationItemBinding.ivStationFavorite.setVisibility(View.VISIBLE);
        fragmentStationsHolder.stationItemBinding.radioItem.setVisibility(View.VISIBLE);
        fragmentStationsHolder.stationItemBinding.radioItem.setClickable(true);
        fragmentStationsHolder.stationItemBinding.setStationItem(station);
        fragmentStationsHolder.stationItemBinding.ivStationItemBackground.setImageResource(station.image);
        fragmentStationsHolder.stationItemBinding.ivStationItemLogo.setImageResource(station.image_logo);

        if (station.isPlaying() | station.isPaused()) {
            if (station.isPlaying()) {
                fragmentStationsHolder.stationItemBinding.ivStationPlay.setBackgroundResource(R.drawable.image_onair_3x);
            } else {
                fragmentStationsHolder.stationItemBinding.ivStationPlay.setBackgroundResource(R.drawable.image_mute_3x);
            }
            fragmentStationsHolder.stationItemBinding.ivStationItemLogo.setAlpha(1f);
            fragmentStationsHolder.stationItemBinding.ivStationPlay.setVisibility(View.VISIBLE);
        } else {
            fragmentStationsHolder.stationItemBinding.ivStationItemLogo.setAlpha(0.5f);
            fragmentStationsHolder.stationItemBinding.ivStationPlay.setVisibility(View.INVISIBLE);
        }


        if (station.favorite) {
            fragmentStationsHolder.stationItemBinding.ivStationFavorite.setBackground(App.getContext().getResources().getDrawable(R.drawable.button_favorite_small_hl_3x));
        } else {
            fragmentStationsHolder.stationItemBinding.ivStationFavorite.setBackground(App.getContext().getResources().getDrawable(R.drawable.button_favorite_small_3x));
        }

        fragmentStationsHolder.stationItemBinding.ivStationFavorite.setOnClickListener(view -> {
            if (!station.favorite) {
                fragmentStationsHolder.stationItemBinding.ivStationFavorite.setBackground(App.getContext().getResources().getDrawable(R.drawable.button_favorite_small_hl_3x));
                station.favorite = true;
                onImageStarClickListener.onStarClick(station);
            } else {
                fragmentStationsHolder.stationItemBinding.ivStationFavorite.setBackground(App.getContext().getResources().getDrawable(R.drawable.button_favorite_small_3x));
                station.favorite = false;
                onImageStarClickListener.onStarClick(station);
            }

        });

        fragmentStationsHolder.stationItemBinding.radioItem.setOnClickListener(view -> {
            onItemClickListener.onItemClick(station);
        });

    }

    private void setItemVisibility(FragmentStationsHolder fragmentStationsHolder, int visible) {
    }

    public void updateFavoriteState(StationModel station) {
        for (int i = 0; i < stationList.size(); i++) {
            if (stationList.get(i).getId() == station.getId()) {
                stationList.get(i).setFavorite(station.isFavorite());
                notifyItemChanged(i);
            }
        }
    }


    class FragmentStationsHolder extends RecyclerView.ViewHolder {
        StationItemBinding stationItemBinding;

        FragmentStationsHolder(StationItemBinding stationItemBinding) {
            super(stationItemBinding.getRoot());
            this.stationItemBinding = stationItemBinding;
        }
    }

    private void setAdsItemMargin(@NonNull FragmentStationsHolder fragmentStationsHolder, boolean isAdsItemLoaded) {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) fragmentStationsHolder.itemView.getLayoutParams();
        int marginBottom;
        if (isAdsItemLoaded) {
            marginBottom = convertDpToPixel(8, App.getContext());
        } else {
            marginBottom = convertDpToPixel(0, App.getContext());
        }

        layoutParams.setMargins(0, 0, 0, marginBottom);
        fragmentStationsHolder.itemView.setLayoutParams(layoutParams);
    }

    private void setItemMargin(@NonNull FragmentStationsHolder fragmentStationsHolder) {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) fragmentStationsHolder.itemView.getLayoutParams();
        int marginBottom = convertDpToPixel(8, App.getContext());
        layoutParams.setMargins(0, 0, 0, marginBottom);
        fragmentStationsHolder.itemView.setLayoutParams(layoutParams);
    }

    private static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int) (dp * (metrics.densityDpi / 160f));
    }

    public interface OnItemClickListener {
        void onItemClick(StationModel station);
    }

    public interface OnImageStarClickListener {
        void onStarClick(StationModel station);
    }
}
