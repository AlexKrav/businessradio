package infoshell.app.businessradio.adapters;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.databinding.FavoriteStationItemBinding;
import infoshell.app.businessradio.helpers.Constants;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.helpers.SwipeAndDragHelper;
import infoshell.app.businessradio.models.StationModel;

public class FavoritesFragmentAdapter extends RecyclerView.Adapter<FavoritesFragmentAdapter.FavoritesFragmentHolder>
        implements SwipeAndDragHelper.ActionCompletionContract {

    private List<StationModel> stationFavoriteList;
    private OnItemClickListener onItemClickListener;
    private OnItemStarClickListener onItemStarClickListener;
    private ItemTouchHelper itemTouchHelper;
    private OnItemMovedListener onItemMovedListener;
    private String starColor;
    private boolean onAir = true;
    private FavoritesFragmentHolder selectedItem;
    private long previousSelectedItem = -1;
    private int selectedStationId = -1;
    int oldPosition;
    int newPosition;
    private boolean first = false;


    public FavoritesFragmentAdapter() {
        this.stationFavoriteList = new ArrayList<>();
        starColor = SharedPreferencesHelper.getTheme();
    }

    public List<StationModel> getStationList() {
        return stationFavoriteList;
    }

    public void setSelectedStation(int stationId) {
        selectedStationId = stationId;
    }

    public void setPlaybackStatus(boolean isPlayed) {
        onAir = isPlayed;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemStarClickListener(OnItemStarClickListener onItemStarClickListener) {
        this.onItemStarClickListener = onItemStarClickListener;
    }

    public void setItemMovedListener(OnItemMovedListener onItemMovedListener) {
        this.onItemMovedListener = onItemMovedListener;
    }

    public void setTouchHelper(ItemTouchHelper itemTouchHelper) {
        this.itemTouchHelper = itemTouchHelper;
    }

    public void refill(List<StationModel> stationFavoriteList) {
        this.stationFavoriteList.clear();
        this.stationFavoriteList.addAll(stationFavoriteList);
        notifyDataSetChanged();
    }

    public void setStarColor(String starColor) {
        this.starColor = starColor;
        notifyDataSetChanged();
    }

    public void clearStations() {
        stationFavoriteList.clear();
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return stationFavoriteList == null ? 0 : stationFavoriteList.size();
    }

    @NonNull
    @Override
    public FavoritesFragmentHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        FavoriteStationItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.favorite_station_item, viewGroup, false);
        return new FavoritesFragmentAdapter.FavoritesFragmentHolder(binding);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull FavoritesFragmentHolder favoritesFragmentHolder, int i) {
        StationModel favoriteStation = stationFavoriteList.get(i);
        favoritesFragmentHolder.favoriteStationItemBinding.setFavoriteStationItem(favoriteStation);

        if (starColor != null) {
            if (starColor.equals(Constants.THEME_RED)) {
                int color = Color.parseColor("#eb485e");
                favoritesFragmentHolder.favoriteStationItemBinding.ivFavoriteStationItemStar.setColorFilter(color);
            } else {
                int color = Color.parseColor("#63adcf");
                favoritesFragmentHolder.favoriteStationItemBinding.ivFavoriteStationItemStar.setColorFilter(color);
            }
        }
        if (favoriteStation.isPlaying() | favoriteStation.isPaused()) {
            if (favoriteStation.isPlaying()) {
                favoritesFragmentHolder.favoriteStationItemBinding.tvOnAir.setVisibility(View.VISIBLE);
            } else {
                favoritesFragmentHolder.favoriteStationItemBinding.tvOnAir.setVisibility(View.INVISIBLE);
            }
            favoritesFragmentHolder.favoriteStationItemBinding.favoriteItem.setBackgroundColor(App.getContext().getResources().getColor(R.color.favorite_cheked_item));
        } else {
            favoritesFragmentHolder.favoriteStationItemBinding.tvOnAir.setVisibility(View.INVISIBLE);
            favoritesFragmentHolder.favoriteStationItemBinding.favoriteItem.setBackgroundColor(App.getContext().getResources().getColor(R.color.transparentBlackBackground));
        }

        favoritesFragmentHolder.favoriteStationItemBinding.ivFavoriteStationItemStar.setOnClickListener(v -> {
            onItemStarClickListener.onStarClick(favoriteStation);
            stationFavoriteList.remove(i);
            notifyDataSetChanged();
        });

        favoritesFragmentHolder.favoriteStationItemBinding.favoriteItem.setOnClickListener(v -> {
            onItemClickListener.onItemClick(favoriteStation);

        });

        favoritesFragmentHolder.favoriteStationItemBinding.ivReorder.setOnTouchListener((v, event) -> {
            if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                itemTouchHelper.startDrag(favoritesFragmentHolder);
            }
            return true;
        });
    }


    @Override
    public void onViewMoved(int oldPosition, int newPosition) {
        stationFavoriteList.get(oldPosition).setPosition(newPosition);
        stationFavoriteList.get(newPosition).setPosition(oldPosition);
        Collections.swap(stationFavoriteList, oldPosition, newPosition);
        notifyItemMoved(oldPosition, newPosition);

    }


    @Override
    public void onViewSwiped(int position) {

    }

    @Override
    public void onViewMoveFinished() {
//        onItemMovedListener.onItemMoved(stationFavoriteList.get(oldPosition), stationFavoriteList.get(newPosition));
        onItemMovedListener.onItemMoved(stationFavoriteList);
    }

    class FavoritesFragmentHolder extends RecyclerView.ViewHolder {
        FavoriteStationItemBinding favoriteStationItemBinding;

        FavoritesFragmentHolder(FavoriteStationItemBinding favoriteStationItemBinding) {
            super(favoriteStationItemBinding.getRoot());
            this.favoriteStationItemBinding = favoriteStationItemBinding;
        }
    }

    public interface OnItemMovedListener {
//        void onItemMoved(StationModel first, StationModel second);

        void onItemMoved(List<StationModel> stationFavoriteList);
    }

    public interface OnItemClickListener {
        void onItemClick(StationModel station);
    }

    public interface OnItemStarClickListener {
        void onStarClick(StationModel station);
    }
}
