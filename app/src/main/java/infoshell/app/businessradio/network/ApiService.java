package infoshell.app.businessradio.network;

import infoshell.app.businessradio.network.models.cbr.CurrencyResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiService {

    @GET("https://www.cbr-xml-daily.ru/daily_json.js")
    Observable<CurrencyResponse> getCurrencies();
}
