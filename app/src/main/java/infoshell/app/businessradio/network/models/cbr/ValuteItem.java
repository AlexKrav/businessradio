package infoshell.app.businessradio.network.models.cbr;

import com.google.gson.annotations.SerializedName;

import static infoshell.app.businessradio.helpers.Constants.RUB_CURRENCY_CHAR_CODE;

public class ValuteItem {

    @SerializedName("ID")
    private String id;

    @SerializedName("NumCode")
    private String numCode;

    @SerializedName("CharCode")
    private String charCode;

    @SerializedName("Nominal")
    private int nominal;

    @SerializedName("Name")
    private String name;

    @SerializedName("Value")
    private double value;

    @SerializedName("Previous")
    private double previous;

    public String getId() {
        return id;
    }

    public String getNumCode() {
        return numCode;
    }

    public String getCharCode() {
        return charCode;
    }

    public int getNominal() {
        return nominal;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public double getPrevious() {
        return previous;
    }

    public static ValuteItem createRub() {
        ValuteItem rub = new ValuteItem();

        rub.name = RUB_CURRENCY_CHAR_CODE;
        rub.value = 1.0;
        rub.previous = 1.0;
        rub.nominal = 1;
        rub.charCode = RUB_CURRENCY_CHAR_CODE;

        return rub;
    }
}
