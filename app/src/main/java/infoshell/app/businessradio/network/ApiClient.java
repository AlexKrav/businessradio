package infoshell.app.businessradio.network;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import infoshell.app.businessradio.network.models.cbr.CurrencyResponse;
import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private final static String BASE_URL = "https://www.cbr-xml-daily.ru/";

    private static ApiClient apiClient;
    private final ApiService apiService;

    public static ApiClient getInstance() {
        if (apiClient == null)
            apiClient = new ApiClient();
        return apiClient;
    }

    private ApiClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .client(client)
                .build();

        apiService = retrofit.create(ApiService.class);
    }

    public Observable<CurrencyResponse> getCurrencies() {
        return apiService.getCurrencies();
    }
}
