package infoshell.app.businessradio.network.models.cbr;

import com.google.gson.annotations.SerializedName;

public class Valute {

    @SerializedName("AUD")
    private ValuteItem aud;

    @SerializedName("AZN")
    private ValuteItem azn;

    @SerializedName("GBP")
    private ValuteItem gbp;

    @SerializedName("AMD")
    private ValuteItem amd;

    @SerializedName("BYN")
    private ValuteItem byn;

    @SerializedName("BGN")
    private ValuteItem bgn;

    @SerializedName("BRL")
    private ValuteItem brl;

    @SerializedName("HUF")
    private ValuteItem huf;

    @SerializedName("HKD")
    private ValuteItem hkd;

    @SerializedName("DKK")
    private ValuteItem ddk;

    @SerializedName("USD")
    private ValuteItem usd;

    @SerializedName("EUR")
    private ValuteItem eur;

    @SerializedName("INR")
    private ValuteItem inr;

    @SerializedName("KZT")
    private ValuteItem kzt;

    @SerializedName("CAD")
    private ValuteItem cad;

    @SerializedName("KGS")
    private ValuteItem kgs;

    @SerializedName("CNY")
    private ValuteItem cny;

    @SerializedName("MDL")
    private ValuteItem mdl;

    @SerializedName("NOK")
    private ValuteItem nok;

    @SerializedName("PLN")
    private ValuteItem pln;

    @SerializedName("RON")
    private ValuteItem ron;

    @SerializedName("XDR")
    private ValuteItem xdr;

    @SerializedName("SGD")
    private ValuteItem sgd;

    @SerializedName("TJS")
    private ValuteItem tjs;

    @SerializedName("TRY")
    private ValuteItem trY;

    @SerializedName("TMT")
    private ValuteItem tmt;

    @SerializedName("UZS")
    private ValuteItem uzs;

    @SerializedName("UAH")
    private ValuteItem uah;

    @SerializedName("CZK")
    private ValuteItem czk;

    @SerializedName("SEK")
    private ValuteItem sek;

    @SerializedName("CHF")
    private ValuteItem chf;

    @SerializedName("ZAR")
    private ValuteItem zar;

    @SerializedName("KRW")
    private ValuteItem krw;

    @SerializedName("JPY")
    private ValuteItem jpy;

    public ValuteItem getAud() {
        return aud;
    }

    public ValuteItem getAzn() {
        return azn;
    }

    public ValuteItem getGbp() {
        return gbp;
    }

    public ValuteItem getAmd() {
        return amd;
    }

    public ValuteItem getByn() {
        return byn;
    }

    public ValuteItem getBgn() {
        return bgn;
    }

    public ValuteItem getBrl() {
        return brl;
    }

    public ValuteItem getHuf() {
        return huf;
    }

    public ValuteItem getHkd() {
        return hkd;
    }

    public ValuteItem getDdk() {
        return ddk;
    }

    public ValuteItem getUsd() {
        return usd;
    }

    public ValuteItem getEur() {
        return eur;
    }

    public ValuteItem getInr() {
        return inr;
    }

    public ValuteItem getKzt() {
        return kzt;
    }

    public ValuteItem getCad() {
        return cad;
    }

    public ValuteItem getKgs() {
        return kgs;
    }

    public ValuteItem getCny() {
        return cny;
    }

    public ValuteItem getMdl() {
        return mdl;
    }

    public ValuteItem getNok() {
        return nok;
    }

    public ValuteItem getPln() {
        return pln;
    }

    public ValuteItem getRon() {
        return ron;
    }

    public ValuteItem getXdr() {
        return xdr;
    }

    public ValuteItem getSgd() {
        return sgd;
    }

    public ValuteItem getTjs() {
        return tjs;
    }

    public ValuteItem getTrY() {
        return trY;
    }

    public ValuteItem getTmt() {
        return tmt;
    }

    public ValuteItem getUzs() {
        return uzs;
    }

    public ValuteItem getUah() {
        return uah;
    }

    public ValuteItem getCzk() {
        return czk;
    }

    public ValuteItem getSek() {
        return sek;
    }

    public ValuteItem getChf() {
        return chf;
    }

    public ValuteItem getZar() {
        return zar;
    }

    public ValuteItem getKrw() {
        return krw;
    }

    public ValuteItem getJpy() {
        return jpy;
    }

    public static ValuteItem getRub() {
        return ValuteItem.createRub();
    }
}
