package infoshell.app.businessradio.fragment_currencies;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.models.CurrencyViewModel;
import infoshell.app.businessradio.network.ApiClient;
import infoshell.app.businessradio.network.models.cbr.CurrencyResponse;
import infoshell.app.businessradio.network.models.cbr.ValuteItem;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CurrenciesFragmentPresenter implements CurrenciesFragmentPresenterInterface {
    private CurrenciesFragmentInterface view;

    public CurrenciesFragmentPresenter(CurrenciesFragmentInterface view) {
        this.view = view;
    }

    @Override
    public void getCurrencyList() {

    }

    @Override
    public void onViewCreated() {
        getCurrenciesToday();
    }

    @Override
    public void onRefreshContent() {
        getCurrenciesToday();
    }

    private void getCurrenciesToday() {
        view.showProgressBar();

        ApiClient.getInstance().getCurrencies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CurrenciesObserver(this));
    }

    private void onSuccessLoadCurrenciesData(CurrencyResponse currencyResponse) {
        view.hideProgressBar();

        if (currencyResponse == null) {
            view.showErrorLoadCurrencies();
            return;
        }

        List<CurrencyViewModel> currencyViewModelList = new ArrayList<>();
        currencyViewModelList.add(getCurrencyViewModel(currencyResponse.getValute().getEur(), currencyResponse.getValute().getUsd()));
        currencyViewModelList.add(getCurrencyViewModel(currencyResponse.getValute().getGbp(), currencyResponse.getValute().getUsd()));
        currencyViewModelList.add(getCurrencyViewModel(currencyResponse.getValute().getUsd(), ValuteItem.createRub()));
        currencyViewModelList.add(getCurrencyViewModel(currencyResponse.getValute().getEur(), ValuteItem.createRub()));
        currencyViewModelList.add(getCurrencyViewModel(currencyResponse.getValute().getUsd(), currencyResponse.getValute().getJpy()));
        currencyViewModelList.add(getCurrencyViewModel(currencyResponse.getValute().getUsd(), currencyResponse.getValute().getChf()));
        currencyViewModelList.add(getCurrencyViewModel(currencyResponse.getValute().getAud(), currencyResponse.getValute().getUsd()));

        view.showCurrencies(currencyViewModelList);
    }

    private void onErrorLoadingCurrenciesData() {
        view.showErrorLoadCurrencies();
        view.hideProgressBar();
    }

    private CurrencyViewModel getCurrencyViewModel(ValuteItem firstCurrency, ValuteItem secondCurrency) {
        CurrencyViewModel currencyViewModel = new CurrencyViewModel();

        currencyViewModel.setTitle(App.getContext().getString(R.string.currency_title, firstCurrency.getCharCode(), secondCurrency.getCharCode()));

        currencyViewModel.setValue(firstCurrency.getValue() / secondCurrency.getValue());

        double dif = (firstCurrency.getValue() / secondCurrency.getValue()) -
                (firstCurrency.getPrevious() / secondCurrency.getPrevious());
        currencyViewModel.setDifference(dif);

        double percent = ((firstCurrency.getValue() / secondCurrency.getValue()) /
                (firstCurrency.getPrevious() / secondCurrency.getPrevious()) - 1) * 100;
        currencyViewModel.setPercent(percent);

        return currencyViewModel;
    }

    private static class CurrenciesObserver implements Observer<CurrencyResponse> {
        private WeakReference<CurrenciesFragmentPresenter> weakMain;

        public CurrenciesObserver(CurrenciesFragmentPresenter weakMain) {
            this.weakMain = new WeakReference<>(weakMain);
        }

        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onNext(CurrencyResponse currencyResponse) {

            CurrenciesFragmentPresenter thiz = weakMain.get();
            if (thiz == null) {
                return;
            }

            thiz.onSuccessLoadCurrenciesData(currencyResponse);
        }

        @Override
        public void onError(Throwable e) {
            CurrenciesFragmentPresenter thiz = weakMain.get();
            if (thiz == null) {
                return;
            }

            thiz.onErrorLoadingCurrenciesData();
        }

        @Override
        public void onComplete() {

        }
    }
}
