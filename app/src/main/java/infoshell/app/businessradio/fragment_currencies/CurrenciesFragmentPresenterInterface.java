package infoshell.app.businessradio.fragment_currencies;

public interface CurrenciesFragmentPresenterInterface {

    void getCurrencyList();

    void onViewCreated();

    void onRefreshContent();
}
