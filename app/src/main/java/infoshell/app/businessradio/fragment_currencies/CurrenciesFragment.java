package infoshell.app.businessradio.fragment_currencies;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import infoshell.app.businessradio.R;
import infoshell.app.businessradio.adapters.CurrencyAdapter;
import infoshell.app.businessradio.databinding.FragmentCurrenciesBinding;
import infoshell.app.businessradio.models.CurrencyViewModel;

public class CurrenciesFragment extends Fragment implements CurrenciesFragmentInterface {
    private FragmentCurrenciesBinding binding;
    private CurrenciesFragmentPresenterInterface presenter;

    private CurrencyAdapter currencyAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_currencies, container, false);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        configView();

        presenter = new CurrenciesFragmentPresenter(this);

        presenter.onViewCreated();

    }

    @Override
    public void showProgressBar() {
        binding.currencyRv.setVisibility(View.GONE);
        binding.refreshBtn.setVisibility(View.GONE);
        binding.errorTv.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        binding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showCurrencies(List<CurrencyViewModel> currencyViewModelList) {
        setContentVisibility(true);
        currencyAdapter.setCurrencyList(currencyViewModelList);
    }

    @Override
    public void showErrorLoadCurrencies() {
        setContentVisibility(false);
    }

    private void configView() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        binding.currencyRv.setLayoutManager(llm);
        currencyAdapter = new CurrencyAdapter();
        binding.currencyRv.setAdapter(currencyAdapter);

        binding.refreshBtn.setOnClickListener(v -> presenter.onRefreshContent());
    }

    private void setContentVisibility(boolean isContectVisibility) {
        if (isContectVisibility) {
            binding.currencyRv.setVisibility(View.VISIBLE);
            binding.progressBar.setVisibility(View.GONE);
            binding.refreshBtn.setVisibility(View.GONE);
            binding.errorTv.setVisibility(View.GONE);
        } else {
            binding.currencyRv.setVisibility(View.GONE);
            binding.progressBar.setVisibility(View.GONE);
            binding.refreshBtn.setVisibility(View.VISIBLE);
            binding.errorTv.setVisibility(View.VISIBLE);
        }
    }
}
