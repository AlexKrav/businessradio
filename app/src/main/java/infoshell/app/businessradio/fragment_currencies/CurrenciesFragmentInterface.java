package infoshell.app.businessradio.fragment_currencies;

import java.util.List;

import infoshell.app.businessradio.models.CurrencyViewModel;

public interface CurrenciesFragmentInterface {

    void showProgressBar();

    void hideProgressBar();

    void showCurrencies(List<CurrencyViewModel> currencyViewModelList);

    void showErrorLoadCurrencies();
}
