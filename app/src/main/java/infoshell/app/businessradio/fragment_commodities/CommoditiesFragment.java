package infoshell.app.businessradio.fragment_commodities;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import infoshell.app.businessradio.R;
import infoshell.app.businessradio.databinding.FragmentCommoditiesBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommoditiesFragment extends Fragment implements CommoditiesFragmentInterface {
    private FragmentCommoditiesBinding binding;
    private CommoditiesFragmentPresenterInterface presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_commodities, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new CommoditiesFragmentPresenter(this);
    }
}
