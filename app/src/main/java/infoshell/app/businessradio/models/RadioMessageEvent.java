package infoshell.app.businessradio.models;

public class RadioMessageEvent {

    private String stationID;
    private boolean onAir;

    public boolean getOnAir() {
        return onAir;
    }

    public void setOnAir(boolean onAir) {
        this.onAir = onAir;
    }

    public String getstationID() {
        return stationID;
    }

    public int getStationIdInt() {
        try {
            return Integer.parseInt(stationID);
        } catch (Exception e) {
            return -1;
        }
    }

    public void setstationID(String stationId) {
        this.stationID = stationId;
    }


}
