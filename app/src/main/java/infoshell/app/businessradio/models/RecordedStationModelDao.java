package infoshell.app.businessradio.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface RecordedStationModelDao {
    @Query("Select * FROM recordedStation")
    Flowable<List<RecordedStation>> getAllRecordedStations();

    @Query("Delete FROM recordedStation")
    void deleteAllRecorderedStations();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(RecordedStation recordedStation);

    @Delete
    void delete(RecordedStation recordedStation);

    @Update
    void update(RecordedStation recordedStation);

    @Query("SELECT * FROM recordedStation WHERE recordedStation.Id = :id")
    Single<RecordedStation> getRecordedStation(int id);
}
