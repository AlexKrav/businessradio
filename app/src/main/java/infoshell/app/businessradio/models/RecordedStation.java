package infoshell.app.businessradio.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class RecordedStation implements Serializable {

    @ColumnInfo(name = "Id")
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "recorded_station_title")
    public int title;

    @ColumnInfo(name = "changed_title")
    public String changedTitle;

    @ColumnInfo(name = "recorded_station_url")
    public String recorded_station_name_url;

    @ColumnInfo(name = "duration")
    public String duration;

    @ColumnInfo(name = "date")
    public long date;

    @ColumnInfo(name = "storage_path")
    public String storage_path;

    @Ignore
    public boolean isPlaying;

    @Ignore
    public boolean isPaused;

    public boolean isPaused() {
        return isPaused;
    }

    public void setPaused(boolean paused) {
        isPaused = paused;
    }

//    public RecordedStation(int id, int title, int recorded_station_name_url, int recording_time, long date, String storage_path) {
//        this.id = id;
//        this.title = title;
//        this.recorded_station_name_url = recorded_station_name_url;
//        this.recording_time = recording_time;
//        this.date = date;
//        this.storage_path = storage_path;
//    }

//    public RecordedStation(RecordedStation targetRecordedStation) {
//        this.id = targetRecordedStation.id;
//        this.title = targetRecordedStation.title;
//        this.recorded_station_name_url = targetRecordedStation.recorded_station_name_url;
//        this.recording_time = targetRecordedStation.recording_time;
//        this.date = targetRecordedStation.date;
//        this.storage_path = targetRecordedStation.storage_path;
//    }

    public String getChangedTitle() {
        return changedTitle;
    }

    public void setChangedTitle(String changedTitle) {
        this.changedTitle = changedTitle;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public int getId() {
        return id;
    }

    public int getTitle() {
        return title;
    }

    public String getRecordedStationNameUrl() {
        return recorded_station_name_url;
    }

    public String getDuration() {
        return duration;
    }

    public long getDate() {
        return date;
    }

    public String getStoragePath() {
        return storage_path;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public void setRecordedStationNameUrl(String recorded_station_name_url) {
        this.recorded_station_name_url = recorded_station_name_url;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setStoragePath(String storage_path) {
        this.storage_path = storage_path;
    }
}
