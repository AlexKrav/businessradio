package infoshell.app.businessradio.models;

public class ColorMessageEvent {

    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


}
