package infoshell.app.businessradio.models;

public class ThemeModel {
    private String theme;

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
}
