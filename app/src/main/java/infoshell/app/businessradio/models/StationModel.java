package infoshell.app.businessradio.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class StationModel implements Serializable {

    @ColumnInfo(name = "Id")
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "station_image")
    public Integer image;

    @ColumnInfo(name = "image_logo")
    public Integer image_logo;

    @ColumnInfo(name = "station_favorite")
    public boolean favorite;

    @ColumnInfo(name = "station_title")
    public int title;

    @ColumnInfo(name = "position")
    public int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isRecording;

    public void setRecording(boolean isRecording) {
        this.isRecording = isRecording;
    }

    public boolean isRecording() {
        return isRecording;
    }

    public int getUrl() {
        return url;
    }

    public void setUrl(int url) {
        this.url = url;
    }

    @ColumnInfo(name = "station_url")
    public int url;

    private boolean isPlaying;

    public boolean isPaused() {
        return isPaused;
    }

    public void setPaused(boolean paused) {
        isPaused = paused;
    }

    private boolean isPaused;

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public StationModel() {

    }

    public StationModel(int id) {
        this.id = id;
    }

    public StationModel(int id, Integer image, Integer image_logo, boolean favorite, int title, int url, boolean isPlaying) {
        this.id = id;
        this.position = id;
        this.image = image;
        this.image_logo = image_logo;
        this.favorite = favorite;
        this.title = title;
        this.url = url;
        this.isPlaying = isPlaying;
    }

    public StationModel(StationModel targetStation) {
        this.id = targetStation.id;
        this.image = targetStation.image;
        this.image_logo = targetStation.image_logo;
        this.favorite = targetStation.favorite;
        this.title = targetStation.title;
        this.url = targetStation.url;
        this.isPlaying = targetStation.isPlaying;
        this.isPaused = targetStation.isPaused;
        this.isRecording = targetStation.isRecording;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public Integer getImage_logo() {
        return image_logo;
    }

    public void setImage_logo(Integer image_logo) {
        this.image_logo = image_logo;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }
}
