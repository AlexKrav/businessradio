package infoshell.app.businessradio.models;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private MutableLiveData<StationModel> stationModelMutableLiveData;
    private MutableLiveData<RecordedStation> recordedStationMutableLiveData;
    private MutableLiveData<AlarmModel> alarmModelMutableLiveData;
    private MutableLiveData<ThemeModel> themeModelMutableLiveData;
    private MutableLiveData<FavoriteStarModel> favoriteStarModelMutableLiveData;

    public static RecordedStation recordedStation;

    public LiveData<FavoriteStarModel> getFavoriteStarModel() {
        if (favoriteStarModelMutableLiveData == null) {
            favoriteStarModelMutableLiveData = new MutableLiveData<>();
        }
        return favoriteStarModelMutableLiveData;
    }

    public LiveData<ThemeModel> getThemeViewModel() {
        if (themeModelMutableLiveData == null) {
            themeModelMutableLiveData = new MutableLiveData<>();
        }
        return themeModelMutableLiveData;
    }

    public LiveData<AlarmModel> getAlarmViewModel() {
        if (alarmModelMutableLiveData == null) {
            alarmModelMutableLiveData = new MutableLiveData<>();
        }
        return alarmModelMutableLiveData;
    }

    public LiveData<StationModel> getStationViewModel() {
        if (stationModelMutableLiveData == null) {
            stationModelMutableLiveData = new MutableLiveData<>();
        }
        return stationModelMutableLiveData;
    }

    public LiveData<RecordedStation> getRecordedStationViewModel() {
        if (recordedStationMutableLiveData == null) {
            recordedStationMutableLiveData = new MutableLiveData<>();
        }
        return recordedStationMutableLiveData;
    }

    public void setStationViewModel(StationModel stationModel) {
        stationModelMutableLiveData.setValue(stationModel);
    }

    public void setRecordedStation(RecordedStation recordedStation) {
        recordedStationMutableLiveData.setValue(recordedStation);
        SharedViewModel.recordedStation = recordedStation;
    }

    public void setAlarmViewModel(AlarmModel alarmModel) {
        alarmModelMutableLiveData.setValue(alarmModel);
    }

    public void setThemeModel(ThemeModel themeModel) {
        themeModelMutableLiveData.setValue(themeModel);
    }

    public void setFavoriteStarModel(FavoriteStarModel favoriteStarModel) {
        favoriteStarModelMutableLiveData.setValue(favoriteStarModel);
    }
}
