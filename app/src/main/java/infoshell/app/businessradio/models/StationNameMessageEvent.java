package infoshell.app.businessradio.models;

public class StationNameMessageEvent {
    private String stationName;

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationName() {
        return stationName;
    }
}
