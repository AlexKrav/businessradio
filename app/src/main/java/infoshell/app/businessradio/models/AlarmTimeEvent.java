package infoshell.app.businessradio.models;

public class AlarmTimeEvent {
    private static AlarmTimeEvent instance;
    private String time;
    private boolean isTurnOn;
    private String widgetName;
    private String dayOfWeek;
    private String date;

    public static AlarmTimeEvent getAlarmTimeEventInstance(){
        if (instance==null){
            instance = new AlarmTimeEvent();
        }
        return instance;
    }

    public String getWidgetName() {
        return widgetName;
    }

    public void setWidgetName(String widgetName) {
        this.widgetName = widgetName;
    }

    public boolean isTurnOn() {
        return isTurnOn;
    }

    public void setTurnOn(boolean turnOn) {
        isTurnOn = turnOn;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String timeOn) {
        this.time = timeOn;
    }
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}
