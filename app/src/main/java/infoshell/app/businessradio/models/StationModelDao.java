package infoshell.app.businessradio.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface StationModelDao {
    @Query("Select * FROM stationModel")
    Flowable<List<StationModel>>
    getAllStations();

    @Query("Select * FROM stationModel  ORDER BY position")
    Flowable<List<StationModel>>
    getSavedStations();

    @Query("Delete FROM stationmodel")
    void deleteAllStations();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUpdatedStations(List<StationModel> stationFavoriteList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(StationModel station);

    @Delete
    void delete(StationModel station);

    @Update
    void update(StationModel station);


    @Query("SELECT * FROM stationModel WHERE stationModel.Id = :id")
    Single<StationModel> getStation(int id);


}
