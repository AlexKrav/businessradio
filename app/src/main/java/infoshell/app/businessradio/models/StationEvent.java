package infoshell.app.businessradio.models;

public class StationEvent {

    private boolean isPlaying;
    private RecordedStation recordedStation;
    private StationModel stationModel;

    public StationEvent() {
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public RecordedStation getRecordedStation() {
        return recordedStation;
    }

    public void setRecordedStation(RecordedStation recordedStation) {
        this.recordedStation = recordedStation;
    }

    public StationModel getStationModel() {
        return stationModel;
    }

    public void setStationModel(StationModel stationModel) {
        this.stationModel = stationModel;
    }
}
