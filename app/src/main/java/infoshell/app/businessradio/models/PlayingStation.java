package infoshell.app.businessradio.models;

public class PlayingStation {

    private StationModel station;
    private boolean isPlaying;

    public PlayingStation() {

    }

    public PlayingStation(StationModel station, boolean isPlaying) {
        this.station = station;
        this.isPlaying = isPlaying;
    }

    public StationModel getStation() {
        return station;
    }

    public void setStation(StationModel station) {
        this.station = station;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }
}
