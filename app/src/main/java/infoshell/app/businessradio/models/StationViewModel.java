package infoshell.app.businessradio.models;

public class StationViewModel {

    private StationModel station;
    private boolean isPlaying;

    public StationViewModel() {
    }

    public StationModel getStation() {
        return station;
    }

    public void setStation(StationModel station) {
        this.station = station;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }
}
