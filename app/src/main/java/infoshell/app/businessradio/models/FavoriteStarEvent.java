package infoshell.app.businessradio.models;

public class FavoriteStarEvent {
    private StationModel station;


    public StationModel getStation() {
        return station;
    }

    public void setStation(StationModel station) {
        this.station = station;
    }
}
