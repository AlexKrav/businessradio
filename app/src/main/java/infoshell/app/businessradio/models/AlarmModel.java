package infoshell.app.businessradio.models;

import java.io.Serializable;

public class AlarmModel implements Serializable {

    private boolean isAlarmTurnOn;
    private boolean isSleepTurnOn;
    private String time;
    private String widgetName;

    public String getWidgetName() {
        return widgetName;
    }

    public void setWidgetName(String widgetName) {
        this.widgetName = widgetName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String timeOn) {
        this.time = timeOn;
    }

    public boolean isAlarmTurnOn() {
        return isAlarmTurnOn;
    }

    public void setAlarmTurnOn(boolean alarmTurnOn) {
        isAlarmTurnOn = alarmTurnOn;
    }

    public boolean isSleepTurnOn() {
        return isSleepTurnOn;
    }

    public void setSleepTurnOn(boolean sleepTurnOn) {
        isSleepTurnOn = sleepTurnOn;
    }

}
