package infoshell.app.businessradio.fragment_radio;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.helpers.RecordingHelper;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.helpers.view.AppBarStateChangeListener;
import infoshell.app.businessradio.models.FavoriteStarModel;
import infoshell.app.businessradio.models.StationModel;
import infoshell.app.businessradio.models.StationModelDao;
import infoshell.app.businessradio.models.ThemeModel;
import infoshell.app.businessradio.services.PlayerService;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static infoshell.app.businessradio.App.getContext;
import static infoshell.app.businessradio.helpers.Constants.THEME_BLUE;
import static infoshell.app.businessradio.helpers.Constants.THEME_RED;

public class RadioFragmentPresenter implements RadioFragmentPresenterInterface {

    private RadioFragmentInterface view;
    private Disposable disposable;
    private StationModelDao favoriteStationDao;


    RadioFragmentPresenter(RadioFragmentInterface view) {
        this.view = view;
        favoriteStationDao = App.getInstance().getAppDatabase().stationModelDao();
    }


    @Override
    public void onViewCreated() {
        getStationName();
        getFavoriteStation();
        getRecordingState();
    }

    @Override
    public void onAppBarLayoutStateChanged(AppBarStateChangeListener.State state) {
        switch (state) {
            case COLLAPSED:
                view.setAppBarCollapsed();
                break;
            case EXPANDED:
                view.setAppBarExpanded();
                break;
        }
    }

    @Override
    public void onChangeThemeEvent(ThemeModel themeModel) {
        String themeColor = themeModel.getTheme();
        if (themeColor.equals(THEME_RED)) {
            view.setRedTheme();
        } else if (themeColor.equals(THEME_BLUE)) {
            view.setBlueTheme();
        }
    }

    @Override
    public void onChangeFavoriteStarEvent(FavoriteStarModel favoriteStarModel) {
        StationModel sharedViewModel = SharedPreferencesHelper.getLastPlayedStation();
        if (sharedViewModel.getId() == favoriteStarModel.getStation().getId()) {
            view.setFavoriteStar(favoriteStarModel.getStation().isFavorite());
        }
    }

    @Override
    public void onPlayBtnClick() {
        StationModel stationModel = SharedPreferencesHelper.getLastPlayedStation();
        if (stationModel.isRecording) {
            PlayerService.stopRecord();
            RecordingHelper.stopRecording();
            stationModel.setRecording(false);
            SharedPreferencesHelper.setLastPlayedStation(stationModel);
            view.setRecordState(false);
        }
//        StationModel station = SharedPreferencesHelper.getLastPlayedStation();
//        String stationID = String.valueOf(station.id);
//        if (!PlayerService.isPlaying()) {
//            turnOnRadio(stationID);
//            view.onPlayBtnClick(station);
//        } else {
//            turnOffRadio(stationID);
//            view.onPauseBtnClick(station);
//        }
        view.changeStationState(stationModel);
    }

    @Override
    public void onRecordBtnClick() {

        if (SharedPreferencesHelper.getSelectedStation() == null) {
            Toast.makeText(getContext(), "Нельзя записать сохраненную станцию", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!PlayerService.isPlaying()) {
            view.showErrorMessage(getContext().getString(R.string.error_record_player_not_run));
            return;
        }

        StationModel stationModel = SharedPreferencesHelper.getLastPlayedStation();
        stationModel.isRecording = true;
        SharedPreferencesHelper.setLastPlayedStation(stationModel);

        if (PlayerService.isRecording()) {
            stopRecord();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    view.requestWriteStoragePermission();
                } else {
                    startRecord();
                }
            } else {
                startRecord();
            }
        }
    }

    @Override
    public void onFavoriteBtnClick() {

        StationModel station = SharedPreferencesHelper.getLastPlayedStation();
        view.setFavoriteStarViewModel(station);

        if (station.isFavorite()) {
            station.setFavorite(false);
            deleteStationInDB(station);

        } else {
            station.setFavorite(true);
            saveStationInDB(station);
        }
        SharedPreferencesHelper.setLastPlayedStation(station);
    }

    @Override
    public void onRequestPermissionsResult(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startRecord();
        } else {
            view.showErrorMessage(getContext().getString(R.string.write_external_storage_deny));
        }
    }

    @Override
    public void onDestroy() {
        if (disposable != null && !disposable.isDisposed()) disposable.dispose();
    }


    @SuppressLint("CheckResult")
    private void saveStationInDB(StationModel station) {
        Completable.fromAction(() -> favoriteStationDao.insert(station))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    view.setFavoriteStar(true);
                    createFavoriteEvent(true);
                }, (throwable) -> {
                    view.setFavoriteStar(false);
                    createFavoriteEvent(false);
                });
    }

    @SuppressLint("CheckResult")
    private void deleteStationInDB(StationModel station) {
        Completable.fromAction(() -> favoriteStationDao.delete(station))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    view.setFavoriteStar(true);
                    createFavoriteEvent(false);
                }, (throwable) -> {
                    view.setFavoriteStar(false);
                    createFavoriteEvent(true);
                });
    }


    private void startRecord() {
        PlayerService.startRecord();
        RecordingHelper.startRecording();
        view.setRecordState(true);
    }

    private void stopRecord() {
        PlayerService.stopRecord();
        RecordingHelper.stopRecording();
        view.setRecordState(false);
    }

    private void getFavoriteStation() {
        StationModel station = SharedPreferencesHelper.getSelectedStation();
        if (station != null) {
            disposable = favoriteStationDao.getStation(station.getId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((stationFavorite) -> {
                                view.setFavoriteStar(stationFavorite.isFavorite());
                            },
                            (throwable) -> {
                                view.setFavoriteStar(false);
                            });
        } else {
            view.setFavoriteStar(false);
        }
    }

    private void createFavoriteEvent(boolean isAdded) {
        FavoriteStarModel favoriteStarModel = new FavoriteStarModel();
        StationModel station = SharedPreferencesHelper.getLastPlayedStation();
        station.setFavorite(isAdded);
        favoriteStarModel.setStation(station);
//        FavoriteStarEvent favoriteEvent = new FavoriteStarEvent();
//        favoriteEvent.setStation(station);

        view.sendFavoriteMessageEvent(favoriteStarModel);
    }

    private void getRecordingState() {
        view.setRecordState(PlayerService.isRecording());
    }

    private void getStationName() {
        StationModel stationName = SharedPreferencesHelper.getLastPlayedStation();
        view.setStationName(App.getContext().getResources().getString(stationName.title));
    }
}
