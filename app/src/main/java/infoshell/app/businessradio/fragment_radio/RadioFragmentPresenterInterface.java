package infoshell.app.businessradio.fragment_radio;

import infoshell.app.businessradio.helpers.view.AppBarStateChangeListener;
import infoshell.app.businessradio.models.FavoriteStarModel;
import infoshell.app.businessradio.models.ThemeModel;

public interface RadioFragmentPresenterInterface {

    void onViewCreated();

    void onAppBarLayoutStateChanged(AppBarStateChangeListener.State state);

    void onChangeThemeEvent(ThemeModel colorMessageEvent);

    void onChangeFavoriteStarEvent(FavoriteStarModel favoriteStarModel);

    void onPlayBtnClick();

    void onRecordBtnClick();

    void onFavoriteBtnClick();

    void onRequestPermissionsResult(int[] grantResults);

    void onDestroy();
}