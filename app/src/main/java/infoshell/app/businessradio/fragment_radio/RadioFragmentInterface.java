package infoshell.app.businessradio.fragment_radio;

import infoshell.app.businessradio.models.FavoriteStarModel;
import infoshell.app.businessradio.models.RadioMessageEvent;
import infoshell.app.businessradio.models.StationModel;

public interface RadioFragmentInterface {

    void setStationName(String stationName);

    void setAppBarCollapsed();

    void setAppBarExpanded();

    void setRedTheme();

    void setBlueTheme();

    void setFavoriteStar(boolean isChecked);

    void sendRadioMessageEvent(RadioMessageEvent radioMessageEvent);

    void sendFavoriteMessageEvent(FavoriteStarModel favoriteStarModel);

    void startPlayerService();

    void pausePlayerService();

    void showErrorMessage(String message);

    void requestWriteStoragePermission();

    void setRecordState(boolean isRecording);

    void onPauseBtnClick(StationModel station);

    void onPlayBtnClick(StationModel station);

    void changeStationState(StationModel station);

    void setFavoriteStarViewModel(StationModel station);
}
