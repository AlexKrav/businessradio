package infoshell.app.businessradio.fragment_radio;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import infoshell.app.businessradio.R;
import infoshell.app.businessradio.adapters.FavoritesFragmentAdapter;
import infoshell.app.businessradio.adapters.FragmentRadioAdapter;
import infoshell.app.businessradio.databinding.FragmentRadioBinding;
import infoshell.app.businessradio.helpers.Constants;
import infoshell.app.businessradio.helpers.DialogUtility;
import infoshell.app.businessradio.helpers.InternetConnectingManager;
import infoshell.app.businessradio.helpers.PlayerControllerListener;
import infoshell.app.businessradio.helpers.RecordingHelper;
import infoshell.app.businessradio.helpers.SharedPreferencesHelper;
import infoshell.app.businessradio.helpers.view.AppBarStateChangeListener;
import infoshell.app.businessradio.models.FavoriteStarModel;
import infoshell.app.businessradio.models.RadioMessageEvent;
import infoshell.app.businessradio.models.SharedViewModel;
import infoshell.app.businessradio.models.StationModel;

public class RadioFragment extends Fragment implements RadioFragmentInterface {
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 1232;
    private FragmentRadioBinding binding;
    private FavoritesFragmentAdapter adapter;
    private SharedViewModel sharedViewModel;
    private OnFragmentInteractionListener listener;
    private PlayerControllerListener playerControllerListener;

    private RadioFragmentPresenterInterface presenter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter = new RadioFragmentPresenter(this);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_radio, container, false);
        return binding.getRoot();
    }

    @Override
    public void onStart() {

        listener.onFragmentStart();
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        presenter.onViewCreated();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        }
        if (context instanceof PlayerControllerListener) {
            playerControllerListener = (PlayerControllerListener) context;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE) {
            presenter.onRequestPermissionsResult(grantResults);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void setStationName(String stationName) {
        if (stationName.equals(getString(R.string.station_rsn))) {
            binding.tvStationTitle.setTextSize(22);
            binding.tvStationTitle.setText(stationName);
        } else {
            binding.tvStationTitle.setTextSize(24);
            binding.tvStationTitle.setText(stationName);
        }
    }

    @Override
    public void setAppBarCollapsed() {
        if (listener != null) {
            listener.onAppBarCollapsed();
        }
    }

    @Override
    public void setAppBarExpanded() {
        if (listener != null) {
            listener.onAppBarExpanded();
        }
    }

    @Override
    public void setRedTheme() {
        binding.radioTabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.MyRed));
        binding.ivCity.setColorFilter(getResources().getColor(R.color.MyRed));
        if (SharedPreferencesHelper.getLastPlayedStation().isFavorite()) {
            binding.btnFavorite.setColorFilter(getResources().getColor(R.color.MyRed));
            binding.tvFavorite.setTextColor(getResources().getColor(R.color.MyRed));
        }
    }

    @Override
    public void setBlueTheme() {
        binding.radioTabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.MyBlue));
        binding.ivCity.setColorFilter(getResources().getColor(R.color.MyBlue));
        if (SharedPreferencesHelper.getLastPlayedStation().isFavorite()) {
            binding.btnFavorite.setColorFilter(getResources().getColor(R.color.MyBlue));
            binding.tvFavorite.setTextColor(getResources().getColor(R.color.tv_favorite_record));
        }
    }

    @Override
    public void setFavoriteStar(boolean isChecked) {
        if (isChecked) {
            binding.btnFavorite.setImageResource(R.drawable.button_favorite_hl_3x);
            if (SharedPreferencesHelper.getTheme().equals(Constants.THEME_BLUE)) {
                binding.tvFavorite.setTextColor(getResources().getColor(R.color.tv_favorite_record));
            } else {
                binding.btnFavorite.setColorFilter(getResources().getColor(R.color.MyRed));
                binding.tvFavorite.setTextColor(getResources().getColor(R.color.tv_favorite_red));
            }
        } else {
            binding.btnFavorite.setImageResource(R.drawable.button_favorite_small_3x);
            binding.tvFavorite.setTextColor(getResources().getColor(R.color.tv_fragment_radio));
            binding.btnFavorite.setColorFilter(null);
            binding.tvFavorite.setTextColor(getResources().getColor(R.color.tv_fragment_radio));
        }
    }

    @Override
    public void sendRadioMessageEvent(RadioMessageEvent radioMessageEvent) {
        EventBus.getDefault().post(radioMessageEvent);
    }

    @Override
    public void sendFavoriteMessageEvent(FavoriteStarModel favoriteStarModel) {
        sharedViewModel.setFavoriteStarModel(favoriteStarModel);
    }

    @Override
    public void startPlayerService() {
        if (playerControllerListener != null) {
            playerControllerListener.startPlayerService();
        }
    }

    @Override
    public void pausePlayerService() {
        if (playerControllerListener != null) {
            playerControllerListener.pausePlayerService();
        }
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestWriteStoragePermission() {
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public void setRecordState(boolean isRecording) {
        StationModel stationModel = sharedViewModel.getStationViewModel().getValue();
        if (stationModel == null) return;
        if (isRecording) {
            stationModel.setRecording(true);
        } else {
            stationModel.setRecording(false);
        }
        sharedViewModel.setStationViewModel(stationModel);
    }

    @Override
    public void onPauseBtnClick(StationModel station) {
        station.setPlaying(false);
        station.setPaused(true);
        sharedViewModel.setStationViewModel(station);
    }

    @Override
    public void onPlayBtnClick(StationModel station) {
        station.setPlaying(true);
        station.setPaused(false);
        sharedViewModel.setStationViewModel(station);
    }

    @Override
    public void changeStationState(StationModel station) {
        StationModel previousStation = sharedViewModel.getStationViewModel().getValue();
        if (previousStation != null && station.getId() == previousStation.getId() & previousStation.isPlaying()) {
            station.setPlaying(false);
            station.setPaused(true);

        } else {
            station.setPlaying(true);
            station.setPaused(false);
            SharedPreferencesHelper.setSelectedStation(SharedPreferencesHelper.getLastPlayedStation());
        }
        if (InternetConnectingManager.isInternetConnected()) {
            if (station.isPlaying()) {
                startPlayerService();
            } else {
                pausePlayerService();
            }
        } else internetNotConnected();
        sharedViewModel.setStationViewModel(station);
    }

    @Override
    public void setFavoriteStarViewModel(StationModel station) {
        if (station.isPlaying()) {
            station.setPlaying(false);
            station.setPaused(true);
        } else {
            station.setPlaying(true);
            station.setPaused(false);
        }
    }

    private void internetNotConnected() {
        DialogUtility.getAlertDialog(getContext(), getResources().getString(R.string.dialog_title_text), getResources().getString(R.string.dialog_message), getResources().getString(R.string.dialog_btn_ok), new DialogUtility.OnAlertButtonClick() {
            @Override
            public void positiveClick(DialogInterface dialogInterface) {
                super.positiveClick(dialogInterface);
            }
        }).show();
    }

    private void initViews() {
        FragmentRadioAdapter radioAdapter = new FragmentRadioAdapter(getFragmentManager());
        binding.pagerFragmentRadio.setOffscreenPageLimit(3);
        binding.pagerFragmentRadio.setAdapter(radioAdapter);
        binding.radioTabLayout.setupWithViewPager(binding.pagerFragmentRadio);
        binding.btnPlay.setBackgroundResource(R.drawable.button_play_3x);
        setupListeners();
        setupObservers();
        new RecordingHelper(getContext());
    }

    private void setupListeners() {

        binding.btnPlay.setOnClickListener(v -> presenter.onPlayBtnClick());

        binding.btnRecord.setOnClickListener(v -> presenter.onRecordBtnClick());

        binding.btnFavorite.setOnClickListener(v -> presenter.onFavoriteBtnClick());

        binding.appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                presenter.onAppBarLayoutStateChanged(state);
            }
        });
    }

    private void setupObservers() {
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel.class);
        sharedViewModel.getStationViewModel().observe(getViewLifecycleOwner(), stationModel -> {
            if (stationModel == null) return;
//            if (!stationModel.isStar()) {
                setStationName(getResources().getString(stationModel.title));
                if (stationModel.isPlaying()) {
                    binding.btnPlay.setBackgroundResource(R.drawable.button_pause_3x);
                } else {
                    binding.btnPlay.setBackgroundResource(R.drawable.button_play_3x);
                    if (stationModel.isRecording) {
                        stationModel.setRecording(false);
                    }
                }
                if (stationModel.isFavorite()) {
                    setFavoriteStar(true);
                } else setFavoriteStar(false);
                if (stationModel.isRecording) {
                    binding.btnRecord.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_record_hl_3x));
                    binding.tvRecord.setTextColor(getResources().getColor(R.color.tv_red_record));
                    binding.tvRecord.setText(getResources().getString(R.string.recording));
                } else {
                    binding.btnRecord.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_record_3x));
                    binding.tvRecord.setTextColor(getResources().getColor(R.color.tv_fragment_radio));
                    binding.tvRecord.setText(getResources().getString(R.string.record));
                }
        });
        sharedViewModel.getThemeViewModel().observe(getViewLifecycleOwner(), themeModel -> {
            presenter.onChangeThemeEvent(themeModel);
        });

        sharedViewModel.getFavoriteStarModel().observe(getViewLifecycleOwner(), favoriteStarModel -> {
            presenter.onChangeFavoriteStarEvent(favoriteStarModel);
        });
    }

    public interface OnFragmentInteractionListener {

        void onFragmentStart();

        void onAppBarCollapsed();

        void onAppBarExpanded();
    }
}
