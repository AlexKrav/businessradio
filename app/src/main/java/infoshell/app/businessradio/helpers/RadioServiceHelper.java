package infoshell.app.businessradio.helpers;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.services.PlayerService;

public class RadioServiceHelper {
    private PlayerService.PlayerServiceBinder playerServiceBinder;
    private MediaControllerCompat mediaController;
    private MediaControllerCompat.Callback callback;
    private ServiceConnection serviceConnection;

    public RadioServiceHelper() {


        if (callback == null && serviceConnection == null) {
            callback = new MediaControllerCompat.Callback() {
                @SuppressLint("SwitchIntDef")
                @Override
                public void onPlaybackStateChanged(PlaybackStateCompat state) {
                    if (state != null) {
                        switch (state.getState()) {
                            case PlaybackStateCompat.STATE_PLAYING: {

                                break;
                            }
                            case PlaybackStateCompat.STATE_PAUSED: {

                                break;
                            }
                            default: {

                                break;
                            }
                        }
                    }
                }
            };

            serviceConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    playerServiceBinder = (PlayerService.PlayerServiceBinder) service;
                    try {
                        mediaController = new MediaControllerCompat(App.getContext(), playerServiceBinder.getMediaSessionToken());
                        mediaController.registerCallback(callback);
                        callback.onPlaybackStateChanged(mediaController.getPlaybackState());
                    } catch (RemoteException e) {
                        mediaController = null;
                        e.printStackTrace();
                    }
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                    playerServiceBinder = null;
                    if (mediaController != null) {
                        mediaController.unregisterCallback(callback);
                        mediaController = null;
                    }
                }
            };

            App.getContext().bindService(new Intent(App.getContext(), PlayerService.class), serviceConnection, Context.BIND_AUTO_CREATE);

        }
    }
}
