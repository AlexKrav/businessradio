package infoshell.app.businessradio.helpers;

import android.arch.lifecycle.LiveData;

public interface PlayerControllerListener {

    LiveData getLiveData();

    void startPlayerService();

    void pausePlayerService();

    void stopPlayerService();
}
