package infoshell.app.businessradio.helpers;


import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyZone;

import infoshell.app.businessradio.main.MainActivity;

public class AdColonyHelper {
    static final private String APP_ID = "appdd4b25bde2854a86a4";
    static final private String ZONE_ID = "vz3523959b63424ad5bb";

    private static AdColonyInterstitial ad;
    private static AdColonyInterstitialListener listener;

    public static void initAdColony(MainActivity mainActivity) {
        // Construct optional app options object to be sent with configure
        AdColonyAppOptions appOptions = new AdColonyAppOptions()
                .setKeepScreenOn(true);

        // Configure AdColony in your launching Activity's onCreate() method so that cached ads can
        // be available as soon as possible.
        AdColony.configure(mainActivity, appOptions, APP_ID, ZONE_ID);

        // Optional user metadata sent with the ad options in each request
//        AdColonyUserMetadata metadata = new AdColonyUserMetadata()
//                .setUserAge(26)
//                .setUserEducation(AdColonyUserMetadata.USER_EDUCATION_BACHELORS_DEGREE)
//                .setUserGender(AdColonyUserMetadata.USER_MALE);

        // Ad specific options to be sent with request
//        adOptions = new AdColonyAdOptions().setUserMetadata(metadata);

        // Set up listener for interstitial ad callbacks. You only need to implement the callbacks
        // that you care about. The only required callback is onRequestFilled, as this is the only
        // way to get an ad object.
        listener = new AdColonyInterstitialListener() {
            @Override
            public void onRequestFilled(AdColonyInterstitial ad) {
                // Ad passed back in request filled callback, ad can now be shown
                AdColonyHelper.ad = ad;
            }

            @Override
            public void onRequestNotFilled(AdColonyZone zone) {
                // Ad request was not filled
                super.onRequestNotFilled(zone);
            }

            @Override
            public void onOpened(AdColonyInterstitial ad) {
                // Ad opened, reset UI to reflect state change
            }

            @Override
            public void onExpiring(AdColonyInterstitial ad) {
                // Request a new ad if ad is expiring
                AdColony.requestInterstitial(ZONE_ID, this);
            }
        };

        AdColony.requestInterstitial(ZONE_ID, listener);
    }

    public static void showAdColony() {
        try {
            ad.show();
        } catch (Exception e) {

        }
    }
}
