package infoshell.app.businessradio.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import infoshell.app.businessradio.App;

public class InternetConnectingManager {

    public static boolean isInternetConnected() {
        NetworkInfo networkInfo = getActiveNetwork();
        boolean isMobileNetworkConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();
        boolean isWiFiConnected = networkInfo != null && networkInfo.isConnectedOrConnecting() && networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
        if (isMobileNetworkConnected) {
            return true;
        } else return isWiFiConnected;
    }

//    public static boolean isMobileNetworkConnected() {
//        NetworkInfo networkInfo = getActiveNetwork();
//        isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();
//
//    }
//
//    public static boolean isWiFiConnected() {
//        NetworkInfo networkInfo = getActiveNetwork();
//        isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting() && networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
//        return isConnected;
//    }

    private static NetworkInfo getActiveNetwork() {
        ConnectivityManager cm = (ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }
}
