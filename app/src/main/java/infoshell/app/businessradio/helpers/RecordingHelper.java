package infoshell.app.businessradio.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaMetadataRetriever;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Random;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.models.RecordedStation;
import infoshell.app.businessradio.models.RecordedStationModelDao;
import infoshell.app.businessradio.models.StationModel;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static infoshell.app.businessradio.App.getContext;

public class RecordingHelper {
    private static InputStream inputStream = null;
    private static FileOutputStream fileOutputStream;
    private static Context context;
    private static RecordedStationModelDao recordedStationModelDao;
    private static String randomAudioFileName;
    private static Random random;

    public RecordingHelper(Context context) {
        RecordingHelper.context = context;
        recordedStationModelDao = App.getInstance().getAppDatabase().recordedStationModelDao();
        random = new Random();
        randomAudioFileName = getContext().getResources().getString(R.string.alphabet);
    }

    public static void startRecording() {
        Thread readThread = new Thread(() -> {
            StationModel stationModel = SharedPreferencesHelper.getSelectedStation();
            String stationUrl = getContext().getString(stationModel.getUrl());
//            String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/myRecordedStation.mp3";
            String audioSavePathInDevice = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + CreateRandomAudioFileName(5) + "recordedStation.mp3";

            try {
                inputStream = new BufferedInputStream(new URL(stationUrl).openStream());
//                fileOutputStream = App.getContext().openFileOutput("myRecordedStation.mp3", Context.MODE_PRIVATE);
                fileOutputStream = new FileOutputStream(audioSavePathInDevice);

            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("LOG_TAG", "url.openStream()");

            int c;
            try {
                if (inputStream != null) {
                    while ((c = inputStream.read()) != -1) {
                        Log.d("LOG_TAG", String.valueOf(c));
                        fileOutputStream.write(c);
//                        Log.d("LOG_TAG", "saved to" + App.getContext().getFilesDir());
                    }
                }
            } catch (IOException e) {
                //save  to  file

                RecordedStation recordedStation = new RecordedStation();
                recordedStation.setTitle(stationModel.title);
                recordedStation.setRecordedStationNameUrl(stationUrl);
                recordedStation.setStoragePath(audioSavePathInDevice);
                recordedStation.setDate(getDate());
                recordedStation.setDuration(getStationDuration(audioSavePathInDevice));
                recordedStation.setPlaying(false);
                saveRecordedStation(recordedStation);
                showDialog();

                try {
                    Log.d("LOG_TAG", String.valueOf("fileOutputStream size " + fileOutputStream.getChannel().size()));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                e.printStackTrace();
            }
        });
        readThread.start();
    }

    public static void stopRecording() {
        if (inputStream != null) {
            Thread thread = new Thread(() -> {
                try {
                    inputStream.close();
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }
    }

    private static String CreateRandomAudioFileName(int string) {
        StringBuilder stringBuilder = new StringBuilder(string);

        int i = 0;
        while (i < string) {
            stringBuilder.append(randomAudioFileName.
                    charAt(random.nextInt(randomAudioFileName.length())));

            i++;
        }
        return stringBuilder.toString();
    }

    @SuppressLint("SimpleDateFormat")
    private static long getDate() {
        return System.currentTimeMillis();
    }

    private static String getStationDuration(String storagePath) {
        // load data file

        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(storagePath);
        String output;
        String duration = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

        long dur = Long.parseLong(duration);

        String seconds = String.valueOf((dur % 60000) / 1000);
        String minutes = String.valueOf(dur / 60000);

        if (seconds.length() == 1) {
            output = "0" + minutes + ":0" + seconds;
        } else {
            output = ("0" + minutes + ":" + seconds);
        }
        metaRetriever.release();
        return output;
    }

    @SuppressLint("CheckResult")
    private static void saveRecordedStation(RecordedStation recordedStation) {
        Completable.fromAction(() -> recordedStationModelDao.insert(recordedStation))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {

                }, (throwable) -> {

                });
    }

    private static void showDialog() {
        Handler handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                DialogUtility.getAlertDialog(context, context.getResources().getString(R.string.recording_finished), context.getResources().getString(R.string.dialog_btn_ok), new DialogUtility.OnAlertButtonClick() {
                    @Override
                    public void positiveClick(DialogInterface dialogInterface) {
                        super.positiveClick(dialogInterface);
                    }
                }).show();
            }
        };
        Message message = handler.obtainMessage(1, "sdf");
        message.sendToTarget();
    }
}
