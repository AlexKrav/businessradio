package infoshell.app.businessradio.helpers;

public final class Constants {

    //Constants for Stocks fragments
    public static final int STOCKS_FRAGMENT_COUNT = 3;
    public static final int CURRENCIES_FRAGMENT_INDEX = 0;
    public static final int INDICES_FRAGMENT_INDEX = 1;
    public static final int COMMODITIES_FRAGMENT_INDEX = 2;

    //Constants for MainActivity fragments
    public static final int RADIO_FRAGMENT_INDEX = 0;
    public static final int STOCKS_FRAGMENT_INDEX = 1;

    public static final String RUB_CURRENCY_CHAR_CODE = "RUB";

    public static final String THEME_BLUE = "Blue";
    public static final String THEME_RED = "Red";
    public static final String THEME_RED_RU = "Красная";

    public static String BTN_ALARM_PRESSED = "BTN_ALARM_PRESSED";
    public static String BTN_SLEEP_PRESSED = "BTN_SLEEP_PRESSED";
    public static int STATE_PLAYING = 3;

    public static final class IntentConstants {
        public static final String STATION_URL = "STATION_URL";
    }


}
