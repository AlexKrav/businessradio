package infoshell.app.businessradio.helpers;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

public class DialogUtility {

   public static abstract class OnAlertButtonClick {
      public   void negativeClick(DialogInterface dialogInterface) {
            dialogInterface.dismiss();
        }

       public void positiveClick(DialogInterface dialogInterface) {
            dialogInterface.dismiss();
        }

       public void neutralClick(DialogInterface dialogInterface) {
            dialogInterface.dismiss();
        }
    }

    /**
     * OnAlertButtonClick use only positiveClick method
     */
    public static Dialog getAlertDialog(Context context, String title, String message, String positiveButton, OnAlertButtonClick onAlertButtonClick) {
        return getAlertDialog(context, title, message, null, positiveButton, null, onAlertButtonClick);
    }
    public static Dialog getAlertDialog(Context context,  String message, String positiveButton, OnAlertButtonClick onAlertButtonClick) {
        return getAlertDialog(context, message, null, positiveButton, null, onAlertButtonClick);
    }

    /**
     * OnAlertButtonClick use positiveClick and negativeClick methods
     */
    public static Dialog getAlertDialog(Context context, String title, String message, String negativeButton, String positiveButton, OnAlertButtonClick onAlertButtonClick) {
        return getAlertDialog(context, title, message, negativeButton, positiveButton, null, onAlertButtonClick);
    }

    private static Dialog getAlertDialog(Context context, String title, String message, String negativeButton, String positiveButton, String neutralButton, OnAlertButtonClick onAlertButtonClick) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title))
            builder.setTitle(title);
        if (!TextUtils.isEmpty(message))
            builder.setMessage(message);
        if (!TextUtils.isEmpty(negativeButton))
            builder.setNegativeButton(negativeButton, (dialogInterface, i) -> {
                if (onAlertButtonClick != null)
                    onAlertButtonClick.negativeClick(dialogInterface);
            });
        if (!TextUtils.isEmpty(positiveButton))
            builder.setPositiveButton(positiveButton, (dialogInterface, i) -> {
                if (onAlertButtonClick != null)
                    onAlertButtonClick.positiveClick(dialogInterface);
            });
        if (!TextUtils.isEmpty(neutralButton))
            builder.setNeutralButton(neutralButton, (dialogInterface, i) -> {
                if (onAlertButtonClick != null)
                    onAlertButtonClick.neutralClick(dialogInterface);
            });
        return builder.create();
    }
}
