package infoshell.app.businessradio.helpers;

public class AlarmSwitchHelper {
    private static AlarmSwitchHelper instance;

    public static AlarmSwitchHelper getInstance() {
        if (instance == null) {
            instance = new AlarmSwitchHelper();
        }
        return instance;
    }

    private static boolean isAlarmTurnOn = false;
    private static boolean isSleepTurnOn = false;

   static boolean getAlarmTurnOn() {
        return isAlarmTurnOn;
    }

    public static void setAlarmTurnOn(boolean alarmTurnOn) {
        isAlarmTurnOn = alarmTurnOn;
    }

   static boolean getSleepTurnOn() {
        return isSleepTurnOn;
    }

    public static void setSleepTurnOn(boolean sleepTurnOn) {
        isSleepTurnOn = sleepTurnOn;
    }


}
