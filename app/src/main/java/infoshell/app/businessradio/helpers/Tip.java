package infoshell.app.businessradio.helpers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;

public class Tip extends Dialog {

    public Tip(Context context) {
        super(context, R.style.tip_dialog);
        setContentView(R.layout.tip);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        findViewById(R.id.tip_dialog).setOnClickListener(v -> dismiss());
        setFonts();
    }

    private void setFonts() {
        TextView tv_tip_alarm_and_timer = findViewById(R.id.tv_tip_alarm_and_timer);
        TextView tv_tip_currencies_and_quotations = findViewById(R.id.tv_tip_currencies_and_quotations);
        TextView tv_tip_list_of_radio_stations = findViewById(R.id.tv_tip_list_of_radio_stations);
        Typeface face = Typeface.createFromAsset(App.getContext().getAssets(), "fonts/Roboto-Italic.ttf");
        tv_tip_alarm_and_timer.setTypeface(face);
        tv_tip_currencies_and_quotations.setTypeface(face);
        tv_tip_list_of_radio_stations.setTypeface(face);
    }
}
