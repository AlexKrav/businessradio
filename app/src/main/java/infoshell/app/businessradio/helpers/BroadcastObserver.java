package infoshell.app.businessradio.helpers;

import java.util.Observable;

public class BroadcastObserver extends Observable {
    private static BroadcastObserver instance;

    public static BroadcastObserver getInstance() {
        if (instance == null) {
            instance = new BroadcastObserver();
        }
        return instance;
    }

    private BroadcastObserver() {
    }

    public void updateValue(Object data) {
        synchronized (this) {
            setChanged();
            notifyObservers(data);
        }
    }

}
