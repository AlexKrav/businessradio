package infoshell.app.businessradio.helpers;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
import java.util.Locale;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.models.AlarmTimeEvent;
import infoshell.app.businessradio.receivers.AlarmReceiver;

public class ProgressDialog extends Dialog {
    private ImageView btnAlarmClose;
    private ImageView btnSleepClose;
    private AlarmManager alarmManager;
    private TimePicker alarmTimePicker;
    private PendingIntent pendingIntent;
    private TextView title, tvAlarmCLose, tvSleepClose;
    private SwitchCompat switchAlarm;
    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener;

    public ProgressDialog(Context context) {
        super(context, R.style.AppTheme);
        setCancelable(false);
        setContentView(R.layout.progress_alarm);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configView();
        setupListeners();
    }

    private void configView() {
        btnAlarmClose = findViewById(R.id.btn_progress_alarm_close);
        btnSleepClose = findViewById(R.id.btn_progress_sleep_close);
        alarmTimePicker = findViewById(R.id.time_picker);
        alarmTimePicker.setIs24HourView(true);
        title = findViewById(R.id.tv_alarm_sleep);
        tvAlarmCLose = findViewById(R.id.tv_alarm_close);
        tvSleepClose = findViewById(R.id.tv_sleep_close);
        switchAlarm = findViewById(R.id.switch_alarm);
        AlarmSwitchHelper.getInstance();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setupListeners() {
        btnAlarmClose.setOnTouchListener((v, event) -> {
            int eid = event.getAction();
            switch (eid) {
                case MotionEvent.ACTION_DOWN:
                    dismiss();
                    break;
            }
            return true;
        });
        btnSleepClose.setOnTouchListener((v, event) -> {
            int eid = event.getAction();
            switch (eid) {
                case MotionEvent.ACTION_DOWN:
                    dismiss();
                    break;
            }
            return true;
        });

        onCheckedChangeListener = (buttonView, isChecked) -> {
            if (switchAlarm.isChecked()) {
                if (btnAlarmClose.getVisibility() == View.VISIBLE) {
                    AlarmSwitchHelper.setAlarmTurnOn(true);
                    turnOnAlarm(Constants.BTN_ALARM_PRESSED);
                } else {
                    AlarmSwitchHelper.setSleepTurnOn(true);
                    turnOnAlarm(Constants.BTN_SLEEP_PRESSED);
                }
            } else {
                if (btnAlarmClose.getVisibility() == View.VISIBLE) {
                    AlarmSwitchHelper.setAlarmTurnOn(false);
                    turnOffAlarm(Constants.BTN_ALARM_PRESSED);
                } else {
                    AlarmSwitchHelper.setSleepTurnOn(false);
                    turnOffAlarm(Constants.BTN_SLEEP_PRESSED);
                }

            }
        };
        switchAlarm.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    private void turnOffAlarm(String widget) {

        if (widget.equals(Constants.BTN_ALARM_PRESSED)) {
            if (pendingIntent != null) {
                alarmManager.cancel(pendingIntent);
            }
            setTextTime("", false, Constants.BTN_ALARM_PRESSED);
        } else {
            if (pendingIntent != null) {
                alarmManager.cancel(pendingIntent);
            }
            setTextTime("", false, Constants.BTN_SLEEP_PRESSED);
        }
    }

    private void turnOnAlarm(String widget) {
        Integer currentHour = alarmTimePicker.getCurrentHour();
        Integer currentMinute = alarmTimePicker.getCurrentMinute();
        String currentTime = convertCurrentTime(currentHour, currentMinute);

        Calendar calendar = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, currentHour);
        calendar.set(Calendar.MINUTE, currentMinute);
        calendar.set(Calendar.SECOND, 0);
        if (calendar.before(now)) calendar.add(Calendar.DAY_OF_MONTH, 1);

        Intent myIntent = new Intent(App.getContext(), AlarmReceiver.class);
        if (widget.equals(Constants.BTN_ALARM_PRESSED)) {
            myIntent.putExtra("alarm", Constants.BTN_ALARM_PRESSED);
        } else if (widget.equals(Constants.BTN_SLEEP_PRESSED)) {
            myIntent.putExtra("alarm", Constants.BTN_SLEEP_PRESSED);
        }
        if (widget.equals(Constants.BTN_ALARM_PRESSED)) {
            pendingIntent = PendingIntent.getBroadcast(App.getContext(), 0, myIntent, 0);
            alarmManager = (AlarmManager) App.getContext().getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
        } else {
            pendingIntent = PendingIntent.getBroadcast(App.getContext(), 1, myIntent, 0);
            alarmManager = (AlarmManager) App.getContext().getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
        }
        setTextTime(currentTime, true, widget);
    }

    private String convertCurrentTime(Integer currentHour, Integer currentMinute) {
        String minute;
        if (currentMinute < 10) {
            minute = "0" + String.valueOf(currentMinute);
        } else {
            minute = String.valueOf(currentMinute);
        }
        return String.valueOf(currentHour + ":" + minute);
    }

    private void setTextTime(String time, boolean isTurnOn, String widgetName) {
        String language = Locale.getDefault().getDisplayLanguage();
        Locale currentLocale = App.getContext().getResources().getConfiguration().locale;
        if (language.equals("русский")) {
            postTextTime(DateFormater.RUSSIAN_DATE_MASK, time, widgetName, isTurnOn, currentLocale);
        } else {
            postTextTime(DateFormater.ENGLISH_DATE_MASK, time, widgetName, isTurnOn, currentLocale);
        }
    }

    private void postTextTime(String mask, String time, String widgetName, boolean isTurnOn, Locale currentLocale) {
        String weekday_name = DateFormater.getDate(currentLocale);
        String date = DateFormater.getDate(mask, currentLocale, time);
        AlarmTimeEvent alarmTimeEvent = AlarmTimeEvent.getAlarmTimeEventInstance();
        alarmTimeEvent.setTime(time);
        alarmTimeEvent.setTurnOn(isTurnOn);
        alarmTimeEvent.setWidgetName(widgetName);
        alarmTimeEvent.setDayOfWeek(weekday_name.substring(0, 1).toUpperCase() + weekday_name.substring(1).toLowerCase());
        alarmTimeEvent.setDate(date);
        EventBus.getDefault().post(alarmTimeEvent);
    }

    public void setVisibilityAlarm() {
        btnAlarmClose.setVisibility(View.VISIBLE);
        tvAlarmCLose.setVisibility(View.VISIBLE);
        btnSleepClose.setVisibility(View.INVISIBLE);
        tvSleepClose.setVisibility(View.INVISIBLE);
        title.setText(R.string.alarm_title);
    }

    public void setVisibilitySleep() {
        btnSleepClose.setVisibility(View.VISIBLE);
        tvSleepClose.setVisibility(View.VISIBLE);
        btnAlarmClose.setVisibility(View.INVISIBLE);
        tvAlarmCLose.setVisibility(View.INVISIBLE);
        title.setText(R.string.Turn_off_title);

    }

    public void setAlarmSwitch() {
        boolean alarmTurnOn = AlarmSwitchHelper.getAlarmTurnOn();
        boolean sleepTurnOn = AlarmSwitchHelper.getSleepTurnOn();
        if (btnAlarmClose.getVisibility() == View.VISIBLE) {
            if (alarmTurnOn) {
                switchAlarm.setOnCheckedChangeListener(null);
                switchAlarm.setChecked(true);
                switchAlarm.setOnCheckedChangeListener(onCheckedChangeListener);
            } else switchAlarm.setChecked(false);
        } else {
            if (sleepTurnOn) {
                switchAlarm.setOnCheckedChangeListener(null);
                switchAlarm.setChecked(true);
                switchAlarm.setOnCheckedChangeListener(onCheckedChangeListener);
            } else switchAlarm.setChecked(false);
        }
    }
}
