package infoshell.app.businessradio.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import infoshell.app.businessradio.App;

public class DateFormater {
    private static final String DATE_PATTERN = "EEEE, dd MMMM, yyyy";
    static final String RUSSIAN_DATE_MASK = "d MMMM yyyy ";
    static final String ENGLISH_DATE_MASK = "d MMMM yyyy h:mm a ";
    private static final String DAY_OF_WEEK = "EEEE";
    private static final String RECORDED_TIME_ENGLISH = "dd MMM yyyy in hh:mm:ss";
    private static final String RECORDED_TIME_RUSSIAN = "dd MMM yyyy в hh:mm:ss";

    public static String getDate(Calendar calendar, Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN, locale);
        return sdf.format(calendar.getTime());
    }

    static String getDate(String mask, Locale locale, String time) {
        SimpleDateFormat sdf = new SimpleDateFormat(mask, locale);
        return sdf.format(System.currentTimeMillis()) + time;
    }

    static String getDate(Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormater.DAY_OF_WEEK, locale);
        return sdf.format(System.currentTimeMillis());
    }

    static String getDate() {
        String language = Locale.getDefault().getDisplayLanguage();
        Locale currentLocale = App.getContext().getResources().getConfiguration().locale;
        SimpleDateFormat sdf;
        if (language.equals("русский")) {
            sdf = new SimpleDateFormat(DateFormater.RECORDED_TIME_RUSSIAN, currentLocale);
        } else {
            sdf = new SimpleDateFormat(DateFormater.RECORDED_TIME_ENGLISH, currentLocale);
        }

        return sdf.format(System.currentTimeMillis());
    }
}
