package infoshell.app.businessradio.helpers;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import infoshell.app.businessradio.App;
import infoshell.app.businessradio.R;
import infoshell.app.businessradio.fragment_station.StationsListHolder;
import infoshell.app.businessradio.models.RecordedStation;
import infoshell.app.businessradio.models.StationModel;

import static infoshell.app.businessradio.helpers.Constants.THEME_BLUE;

public class SharedPreferencesHelper {
    private static final String THEME_COLOR = "theme_color";
    private static final String SELECTED_STATION = "selected_station";
    private static final String LAST_STATION_PLAYED_ID = "LAST_STATION_PLAYED_ID";
    private static final String LAST_STATION_NAME = "LAST_STATION_NAME";
    private static final String TIPS = "TIPS";
    private static final String RECORDED_STATION = "RECORDED_STATION";
    private static final String IS_FIRST_START = "IS_FIRST_START";
    private static final String IS_FIRST_START_LAST_PLAYED = "IS_FIRST_START_LAST_PLAYED";
    private static final String LAST_PLAYED_STATION = "LAST_PLAYED_STATION";
    private static final String ALARM_ON = "ALARM_ON";
    private static final String ALARM_OFF = "ALARM_OFF";
    private static final String BTN_ALARM_PRESSED = "BTN_ALARM_PRESSED";

    public static String IsBtnAlarmPressed() {
        return getPreferences().getString(BTN_ALARM_PRESSED, null);
    }

    public static void setBtnAlarmPressed(String alarmPressed) {
        getEditor().putString(BTN_ALARM_PRESSED, alarmPressed).apply();
    }

    public static String IsAlarmOff() {
        return getPreferences().getString(ALARM_OFF, null);
    }

    public static void setAlarmOff(String alarmOff) {
        getEditor().putString(ALARM_OFF, alarmOff).apply();
    }

    public static String IsAlarmOn() {
        return getPreferences().getString(ALARM_ON, null);
    }

    public static void setAlarmOn(String alarmOn) {
        getEditor().putString(ALARM_ON, alarmOn).apply();
    }

    public static RecordedStation getRecordedStation() {
        RecordedStation recordedStation = null;
        Gson gson = new Gson();
        String json = getPreferences().getString(RECORDED_STATION, null);
        if (json != null) {
//            json = gson.toJson(json);
            recordedStation = gson.fromJson(json, RecordedStation.class);
        }
        return recordedStation;
    }

    public static void setRecordedStation(RecordedStation recordedStation) {
        if (recordedStation == null) {
            getEditor().putString(RECORDED_STATION, null).apply();
        } else {
            Gson gson = new Gson();
            String json = gson.toJson(recordedStation);
            getEditor().putString(RECORDED_STATION, json).apply();
        }
    }

    public static void setFirstAppStart(String isFirstStart) {
        getEditor().putString(TIPS, isFirstStart).apply();
    }

    public static String isFirstStart() {
        return getPreferences().getString(TIPS, "");
    }

    public static void setSelectedStation(StationModel station) {
        if (station == null) {
            getEditor().putString(SELECTED_STATION, null).apply();
        } else {
            Gson gson = new Gson();
            String json = gson.toJson(station);
            getEditor().putString(SELECTED_STATION, json).apply();
        }
    }

    public static void setLastPlayedStation(StationModel station) {
        Gson gson = new Gson();
        String json = gson.toJson(station);
        getEditor().putString(LAST_PLAYED_STATION, json).apply();
    }

    public static StationModel getLastPlayedStation() {
        StationModel stationModel = null;
        Gson gson = new Gson();
        String json = getPreferences().getString(LAST_PLAYED_STATION, null);

        if (getPreferences().getString(IS_FIRST_START_LAST_PLAYED, null) == null) {
            getEditor().putString(IS_FIRST_START_LAST_PLAYED, "isFirstStartLastPlayed").apply();
            SharedPreferencesHelper.setLastPlayedStation(StationsListHolder.getStationModels().get(0));
            return StationsListHolder.getStationModels().get(0);
        }
        if (json != null) {
//            json = gson.toJson(StationsListHolder.getStationModels().get(0));
            stationModel = gson.fromJson(json, StationModel.class);
        }

        return stationModel;
    }

    public static StationModel getSelectedStation() {
        StationModel stationModel = null;
        Gson gson = new Gson();
        String json = getPreferences().getString(SELECTED_STATION, null);

        if (getPreferences().getString(IS_FIRST_START, null) == null) {
            getEditor().putString(IS_FIRST_START, "isFirstStart").apply();
            SharedPreferencesHelper.setSelectedStation(StationsListHolder.getStationModels().get(0));
            return StationsListHolder.getStationModels().get(0);
        }
        if (json != null) {
//            json = gson.toJson(StationsListHolder.getStationModels().get(0));
            stationModel = gson.fromJson(json, StationModel.class);
        }


        return stationModel;

    }

//    Gson gson = new Gson();
//    String json = getPreferences().getString(SELECTED_STATION, "");
//        if (json.isEmpty()) {
//        json = gson.toJson(StationsListHolder.getStationModels().get(0));
//    }
//        return gson.fromJson(json, StationModel.class);

    public static void setTheme(String theme) {
        getEditor().putString(THEME_COLOR, theme).apply();
    }

    public static String getTheme() {
        return getPreferences().getString(THEME_COLOR, THEME_BLUE);
    }

    public static void setLastStationPlayedId(String id) {
        getEditor().putString(LAST_STATION_PLAYED_ID, id).apply();
    }

    public static String getLastStationPlayedId() {
        return getPreferences().getString(LAST_STATION_PLAYED_ID, "1");
    }

//    public static void setLastStationName(String name) {
//        getEditor().putString(LAST_STATION_NAME, name).apply();
//    }
//
//    public static String getLastStationName() {
//        return getPreferences().getString(LAST_STATION_NAME, App.getContext().getString(R.string.station_business_fm));
//    }

    public static void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener sharedPreferences) {
        getPreferences().registerOnSharedPreferenceChangeListener(sharedPreferences);
    }

    private static SharedPreferences.Editor getEditor() {
        return getPreferences().edit();
    }

    private static SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(App.getContext());
    }
}
